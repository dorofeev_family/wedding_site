/**
 * Created by dits on 03.07.15.
 */
function sendComment(){
    str = $("#comment-form").serialize() + "&ajax=comment-form&news_id="+news_id;

    $.ajax({
        type: "POST",
        url: "/news/addcomment",
        data: str,
        dataType: "json",
        beforeSend : function() {
            $("#addcomment").attr("disabled",true);
        },
        success: function(data) {
            $("#addcomment").attr("disabled",false);

            if (data.success){
                refreshComments(data.html);
                swal({
                    title: data.text,
                    allowOutsideClick: true,
                    type: "success",
                    timer: 2000,
                    confirmButtonText: "Ок"
                });
            } else {
                swal({
                    title: data.text,
                    allowOutsideClick: true,
                    text: data.errors.text[0],
                    type: "error",
                    confirmButtonText: "Ок"
                });
            }

        }
    });
    return false;
}

function refreshComments(html){
    $('#outer-comment-box').html(html);
}

function navigate(elementId) {
    $('html, body').animate({ scrollTop: $("#"+elementId).position().top-150 }, 1000);
}

function saveAsParent(elem, id){
    $(".parent-comment").removeClass("parent-comment");
    var user = $(elem).find('.comment_user:first').html();
    var date = $(elem).find('.comment_date:first').html()
    var message = "Ответ на сообщение пользователя " + user + " от <b>" + date + "</b>"
    var button = "   <a class='remove-parent-link' onclick='removeFromParent();' > x </a>";

    $("form input.comment-parent-id").val(id);
    $("form #parent-info").html(message + button);
    $(elem).addClass("parent-comment");
    navigate('comment-form-box');
}

function removeFromParent(){
    $("form input.comment-parent-id").val('');
    $("form #parent-info").html('');
    $(".parent-comment").removeClass("parent-comment");
}

$( document ).ready(function(){
    $.ajax({
        url: "/news/GetNewsComments/"+news_id,
        success: function(data) {
            refreshComments(data);
        }
    });
});

function editComment(id, event){
    stopEvent(event);

    var comment = $('#comment-id-'+id);
    var originComment = $(comment).find('.comment_text p').text();

    var modal = $("#edit-comment-modal");

    $(modal).find('#comment-form-box .comment_user').html($(comment).find('.comment_user').html());
    $(modal).find('#comment-form-box .comment_date').html($(comment).find('.comment_date').html());
    $(modal).find('#comment-form-box .comment_id').html(id);
    $(modal).find('#comment-form-box #comment_text').html(originComment);

    $(modal).modal("show");
}

function sendEditCommentRequest(){
    var modal = $("#edit-comment-modal");
    var id = $(modal).find('#comment-form-box .comment_id').html();
    var inputValue = $(modal).find('#comment-form-box #comment_text').val();

    $.ajax({
        type: "POST",
        url: "/news/editcomment",
        data: {id : id, text : inputValue, news_id : news_id},
        dataType: "json",
        success: function(data) {
            if (data.success){
                swal({
                    title: "Комментарий измен",
                    type: "success",
                    timer: 2000,
                    confirmButtonText: "Ок"
                });
                $("#edit-comment-modal").modal("hide");
                refreshComments(data.html);
            } else {
                swal({
                    title: data.text,
                    allowOutsideClick: true,
                    text: data.errors.text[0],
                    type: "error",
                    confirmButtonText: "Ок"
                });
            }

        }
    });

}

function sendDeleteCommentRequest(id){

    $.ajax({
        type: "POST",
        url: "/news/deletecomment",
        data: {id : id, news_id : news_id},
        dataType: "json",
        success: function(data) {
            if (data.success){
                refreshComments(data.html);
                swal({
                    title: "Комментарий удален",
                    type: "success",
                    timer: 2000,
                    confirmButtonText: "Ок"
                });
            } else {
                swal({
                    title: data.text,
                    allowOutsideClick: true,
                    text: data.error,
                    type: "error",
                    confirmButtonText: "Ок"
                });
            }

        }
    });


}

function deleteComment(id, event){
    stopEvent(event);

    swal({
            title: "Удалить комментарий?",
            text: "Восстановить комментарий будет невозможно!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Удалить!",
            cancelButtonText: "Отмена!",
            closeOnConfirm: false
        },
        function(){
            sendDeleteCommentRequest(id);
        });
}

function stopEvent(e){
    if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();
}