var imageTypes = ['jpeg', 'jpg', 'png']; //Validate the images to show
function showImage(src, target)
{
    var fr = new FileReader();
    fr.onload = function(e)
    {
        target.src = this.result;
    };
    fr.readAsDataURL(src.files[0]);
}

function changeBG(src, target)
{
    var fr = new FileReader();
    fr.onload = function(e)
    {
        //target.src = this.result;
        $(target).css("background-image", "url("+this.result+")");
        //$('#objectID').css('background', 'transparent url('+e.target.result +') left top no-repeat');
    };
    fr.readAsDataURL(src.files[0]);
}

var uploadImage = function(obj)
{
    var val = obj.value;
    var lastInd = val.lastIndexOf('.');
    var ext = val.slice(lastInd + 1, val.length);
    if (imageTypes.indexOf(ext) !== -1)
    {
        var id = $(obj).data('target');
        var src = obj;
        var target = $(id)[0];
        var preview = $('#newsMainImage')[0];
        var bg = $('.news-main-image-bg')[0];
        showImage(src, target);
        showImage(src, preview);
        changeBG(src, bg);
        $('#deleteImageFlagInput').val('');
    }
    else
    {

    }
};

function deleteImage(){
    $('#newsMainImage').attr("src", '/images/no-news-image.png');
    $('#deleteImageFlagInput').val('1');
}