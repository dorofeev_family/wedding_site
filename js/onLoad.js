/**
 * Created by dits on 15.06.15.
 */
var pageHeight;
var fullPageHeight;

function showExpireText(){
    $('.countdown-container #countdown-title h3').html(countdown.expireText);
    $('.countdown-container').addClass('expired');
    $('.countdown-container #defaultCountdown').hide();
}

(function ($){
    $(function () {
        pageHeight = $(window).height();;
        fullPageHeight = $("body")[0].scrollHeight;

        $.countdown.regionalOptions['ru'] = {
            labels: ['Лет', 'Месяцев', 'Недель', 'Дней', 'Часов', 'Минут', 'Секунд'],
            labels1: ['Год', 'Месяц', 'Неделя', 'День', 'Час', 'Минута', 'Секунда'],
            labels2: ['Года', 'Месяца', 'Недели', 'Дня', 'Часа', 'Минуты', 'Секунды'],
            compactLabels: ['л', 'м', 'н', 'д'], compactLabels1: ['г', 'м', 'н', 'д'],
            whichLabels: function(amount) {
                var units = amount % 10;
                var tens = Math.floor((amount % 100) / 10);
                return (amount == 1 ? 1 : (units >= 2 && units <= 4 && tens != 1 ? 2 :
                    (units == 1 && tens != 1 ? 1 : 0)));
            },
            digits: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
            timeSeparator: ':', isRTL: false};
        $.countdown.setDefaults($.countdown.regionalOptions['ru']);


        if (typeof(countdown) != 'undefined'){

            countdown.time = stringToDate(countdown.time);

            $('#countdown-title h3').html(countdown.title);
            var settings;
            if (countdown.type == 'until'){
                settings = {
                    until: countdown.time,
                    format: 'dHMS',
                    onExpiry: showExpireText,
                    alwaysExpire: true
                }
            } else {
                settings = {
                    since: countdown.time,
                    format: 'YOWD'
                }
            }
            $('#defaultCountdown').countdown(settings);
        }

        jQuery(".backtotop").click(function(){ jQuery("html, body").animate({scrollTop:0}, "slow"); return false; });
        jQuery(".backtobottom").click(function(){ jQuery("html, body").animate({scrollTop: fullPageHeight}, "slow"); return false; });

        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash),
                    top = 0;
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    top = parseInt(target.offset().top) - 90;
                    $('html,body').animate({
                        scrollTop: top
                    }, 1000);
                    return false;
                }
            }
        });
        
        $('.left-side-box ul.menu li.spoiler-element > a').click(function () {
            var oldActiveBlock = $('.left-side-box ul.menu li.spoiler-element.active');
            var newActiveBlock = $(this).parent();

            oldActiveBlock.find('> div').slideUp(500, function(){
                oldActiveBlock.find('> a').show();
                oldActiveBlock.removeClass('active');
            });

            newActiveBlock.find('> a').hide();
            newActiveBlock.addClass('active');
            newActiveBlock.find('> div').slideDown(500);
        });


        init2GIS();
    });
})(jQuery);

window.onscroll = function()
{
    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
    if (scrollTop > pageHeight*0.6){
        if (scrollTop > pageHeight) {
            $('#to-top > span').show();
        } else {
            $('#to-top > span').hide();
        }

        if ((fullPageHeight - scrollTop - pageHeight) >= pageHeight) {
            $('#to-bottom > span').show();
        } else {
            $('#to-bottom > span').hide();
        }
    } else {
        $('#to-bottom > span').hide();
        $('#to-top > span').hide();
    }

};

function stringToDate(date){
    var a = date.split(" ");
    var d = a[0].split("-");
    var t = a[1].split(":");
    return new Date(d[0],(d[1]-1),d[2],t[0],t[1],t[2]);
}

function init2GIS(){
    DG.then(function () {
        if ($('#zagsMap').length) {
            var zagsMap = DG.map('zagsMap', {
                'center': [56.477729, 84.95029],
                'zoom': 15,
                'scrollWheelZoom': false,
                'fullscreenControl': false
            });
            DG.marker([56.477729, 84.95029]).addTo(zagsMap);
        }

        if ($('#banketMap').length) {
            var banketMap = DG.map('banketMap', {
                'center': [56.462289, 84.96931],
                'zoom': 16,
                'scrollWheelZoom': false,
                'fullscreenControl': false
            });
            DG.marker([56.462289, 84.96931]).addTo(banketMap);

        }
    });
}