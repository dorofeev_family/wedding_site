$(function () {
    VK.init({
        apiId: 5028139
    });
});

function sendwallpost(link) {
    if (!link){
        link = window.location.pathname;
    }

    VK.api("wall.post", {
        attachments: 'http://xn--b1adda7ban7a7d.xn--p1ai' + window.location.pathname
    }, function (data) {
        if (data.response){
            swal({
                title: 'Запись опубликована в VK',
                type: "success",
                timer: 2000,
                confirmButtonText: "Ок"
            });
        } else {
            swal({
                title: 'Ошибка при опубликовании записи в VK',
                text: data,
                type: "error",
                timer: 2000,
                confirmButtonText: "Ок"
            });
        }
    });
}