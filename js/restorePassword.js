/**
 * Created by dits on 20.07.15.
 */
function restorePassword(){
    var username = $('#username').val();
    if (username){
        $.ajax({
            type: "POST",
            url: "/user/restorePassword",
            data: {username: username},
            dataType: "json",
            success: function(data) {
                if (data.success){
                    swal({
                        title: data.title,
                        text: data.text,
                        allowOutsideClick: true,
                        type: "success",
                        confirmButtonText: "Ок"
                    });
                    $("#restore-password-modal").modal("hide");
                } else {
                    swal({
                        title: data.title,
                        text: data.text,
                        allowOutsideClick: true,
                        type: "error",
                        confirmButtonText: "Ок"
                    });
                }
            }
        });
    } else {
        swal({
            title: "Ошибка",
            allowOutsideClick: true,
            text: 'Имя пользователя не может быть пустым',
            type: "error",
            confirmButtonText: "Ок"
        });
    }


    return false;
}

function showRestorePasswordModal(){
    var modal = $("#restore-password-modal");
    $(modal).modal("show");
}