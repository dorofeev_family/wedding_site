

(function ($){
    $(function () {
        $( "#news-title" ).change(function() {
            $(".single_news .front_news_title h2").html($(this).val());
            $(".single_news .news_title_box .news_title a").html($(this).val());
        });

        CKEDITOR.on( 'instanceReady', function( ev ) {
            if (ev.editor.name == 'preview-textarea'){
                updatePreview();
            }
        });
    });
})(jQuery);

function updatePreview(){
    CKEDITOR.instances['preview-textarea'].on('blur', function(e) {
        if (e.editor.checkDirty()) {
            var data = e.editor.getData();
            console.log(data);
            $(".single_news .news_preview").html(data);
        }
    });
}