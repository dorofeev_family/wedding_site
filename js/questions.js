/**
 * Created by dits on 20.07.15.
 */
function sendQuestion(){

    $.ajax({
        type: "POST",
        url: "/questions/addQuestion",
        data: $("#question-form").serialize(),
        dataType: "json",
        beforeSend : function() {
            $("#addquestion").attr("disabled",true);
        },
        success: function(data) {
            $("#addquestion").attr("disabled",false);

            if (data.success){
                swal({
                    title: data.text,
                    allowOutsideClick: true,
                    type: "success",
                    timer: 2000,
                    confirmButtonText: "Ок"
                });
                $('#question-form textarea.question-text').val('');

            } else {
                swal({
                    title: data.text,
                    allowOutsideClick: true,
                    text: data.errors.question[0],
                    type: "error",
                    confirmButtonText: "Ок"
                });
            }
        }
    });

    return false;
}


function deleteQuestion(id){
    swal({
            title: "Удалить вопрос?",
            text: "Вы уверены, что хотите удалить выбранный вопрос?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Удалить!",
            cancelButtonText: "Отмена!",
            closeOnConfirm: false
        },
        function(){
            sendDeleteCommentRequest(id);
        });
}


function sendDeleteCommentRequest(id){

    $.ajax({
        type: "POST",
        url: "/questions/deleteQuestion",
        data: {id : id},
        dataType: "json",
        success: function(data) {
            if (data.success){
                swal({
                    title: "Вопрос удален",
                    type: "success",
                    timer: 2000,
                    confirmButtonText: "Ок"
                });
                hideQuestion(id);
            } else {
                swal({
                    title: data.text,
                    allowOutsideClick: true,
                    text: data.error,
                    type: "error",
                    confirmButtonText: "Ок"
                });
            }

        }
    });
}

function hideQuestion(id){
    $('div#'+id).slideUp();
}

function refreshQuestions(html){
    $('#outer-questions-box').html(html);
}

function getQuestions(){
    var type = $('#question_filter').val();
    $.ajax({
        url: "/questions/GetQuestionByType/"+type,
        success: function(data) {
            refreshQuestions(data);
        }
    });
}

function showEditModal(id){
    $.ajax({
        type: "POST",
        url: "/questions/GetSingleQuestionInfo/",
        data: {id: id},
        success: function(data) {
            if (data.success){
                var modal = $("#edit-question-modal");

                $(modal).find('#question_id').val(data.question.question.id);
                $(modal).find('#question_username').html(data.question.question_user.FullName);
                $(modal).find('#question_createdate').html(data.question.question.createdate);
                $(modal).find('#question_text').val(data.question.question.question);

                $(modal).find('#answer_username').html(data.answer.answer_user.FullName);
                if (data.answer.answer){
                    $(modal).find('#answer_createdate').html(data.answer.answer.answerDate);
                    $(modal).find('#answer_text').val(data.answer.answer.answer);
                } else {
                    $(modal).find('#answer_createdate').html('');
                    $(modal).find('#answer_text').val('');
                }

                $('#question_status_list [value=' + data.question.question.status_id + ']').attr('selected', 'true');

                $(modal).modal("show");
            } else {
                swal({
                    title: data.text,
                    allowOutsideClick: true,
                    text: data.error,
                    type: "error",
                    confirmButtonText: "Ок"
                });
            }

        }
    });

}

function editQuestion(){
    var str = $("#edit-question-form").serialize();
    $.ajax({
        type:'POST',
        data: str,
        url: '/questions/editQuestion',
        success: function(data){
            if (data.success){
                swal({
                    title: data.text,
                    type: "success",
                    timer: 2000,
                    confirmButtonText: "Ок"
                });
                $('#edit-question-modal').modal("hide");
                getQuestions();
            } else {
                swal({
                    title: data.text,
                    allowOutsideClick: true,
                    text: data.error,
                    type: "error",
                    confirmButtonText: "Ок"
                });
            }
        }
    });
}

function approveQuestion(id){
    changeQuestionStatus(id,1);
}

function undoApproveQuestion(id){
    changeQuestionStatus(id,0);
}

function changeQuestionStatus(id,approve){
    $.ajax({
        type: 'POST',
        url: "/questions/changeQuestionStatus/",
        data: {
            id: id,
            approve: approve,
            type: $('#question_filter').val()
        },
        success: function(data) {
            if (data.success){
                swal({
                    title: data.text,
                    type: "success",
                    timer: 2000,
                    confirmButtonText: "Ок"
                });
                getQuestions();
            } else {
                swal({
                    title: data.text,
                    allowOutsideClick: true,
                    text: data.error,
                    type: "error",
                    confirmButtonText: "Ок"
                });
            }
        }
    });
}

$( document ).ready(function(){
    getQuestions();
});