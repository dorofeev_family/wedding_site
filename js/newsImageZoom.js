/**
 * Created by dits on 13.08.15.
 */

$(function () {
    $('.news-full-data .news_preview img').each(function(index) {
        $(this).addClass("zoom");
        $(this).click(function() {
            showFullSizeImage(this);
        });
    });
});

function showFullSizeImage(img){

    var modal = $("#full-image-modal");

    var src = $(img).attr("src");
    $(modal).find('#fullsize-image').attr("src", src);
    //$(modal).find('#fullsize-image').css("background-image", "url("+src + ")");
    $(modal).find('#fullsize-image').click(function(){
        $(modal).modal("hide");
    });

    $(modal).modal("show");

}