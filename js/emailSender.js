$( document ).ready(function(){
    $("input#use_template_switch").change(function(){
        if ($('#own_text').is(":visible")) {
            $('#use_template').slideDown();
            $('#own_text').slideUp();
        } else {
            $('#use_template').slideUp();
            $('#own_text').slideDown();
        }

    });
});

function sendEmail(){
    str = $("#email-form").serialize();

    $.ajax({
        type: "POST",
        url: "/mail/sendMail",
        data: str,
        dataType: "json",
        beforeSend : function() {
            $("#email-sender-button").attr("disabled",true);
        },
        success: function(data) {
            $("#email-sender-button").attr("disabled",false);

            if (data.success){
                swal({
                    title: data.text,
                    allowOutsideClick: true,
                    type: "success",
                    timer: 2000,
                    confirmButtonText: "Ок"
                });
            } else {
                swal({
                    title: data.text,
                    allowOutsideClick: true,
                    text: data.errors,
                    type: "error",
                    confirmButtonText: "Ок"
                });
            }
        }
    });
    return false;
}


