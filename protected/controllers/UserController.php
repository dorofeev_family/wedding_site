<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	// public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

            array('allow',  // deny all users
                'actions'=>array('ChangePassword'),
                'users'=>array('*'),
            ),

			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
                'roles'=>array(User::ROLE_ADMIN),
			),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create'),
                'roles'=>array(User::ROLE_ADMIN),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('logout'),
                'users'=>array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('signin', 'login', 'restorePassword'),
                'users' => array(User::ROLE_GUEST),
            ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update', 'subscribe'),
                'roles'=>array('profile'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('changerole', 'delete', 'multiDelete'),
                'roles'=>array('m_users'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        $this->pageTitle = "Новый пользователь";

		$user = new User('create');
        $user->user_profile = new UserProfile();

		if(isset($_POST['User'], $_POST['UserProfile'], $_POST['Role']))
		{
            if ($user->saveRelated($_POST))
                $this->redirect(array('index'));
		}

		$this->render('create',array(
			'model' => $user
		));
	}

    public function actionSignin()
    {
        $this->pageTitle = "Регистрация";

        $user = new User('create');
        $user->user_profile = new UserProfile;

        if(isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
        {
            echo CActiveForm::validate($user);
            echo CActiveForm::validate($user->user_profile);
            Yii::app()->end();
        }

        if(isset($_POST['User'], $_POST['UserProfile']))
        {
            if ($user->saveRelated($_POST)){
                $this->loginMe($_POST['User']);
            }
        }

        $this->render('signin',array(
            'model' => $user
        ));
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $model = new LoginForm;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
        {
            $errors = CActiveForm::validate($model);
            if ($errors != '[]')
            {
                echo $errors;
                Yii::app()->end();
            }
        }

        // collect user input data
        if (isset($_POST['LoginForm']))
        {
            $this->loginMe($_POST['LoginForm']);
        }

        $this->render('login',array('model'=>$model));
    }

    private function loginMe($UserData, $url = false){
        $url = ($url) ? ($url) : (Yii::app()->user->returnUrl);
        $loginForm = new LoginForm;
        $loginForm->attributes = $UserData;
        // validate user input and redirect to the previous page if valid
        if ($loginForm->validate() && $loginForm->login())
        {
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
            {
                $redirectUrl = "";

                echo CJSON::encode(array(
                    'authenticated' => true,
                    'redirectUrl' => $redirectUrl,
                ));
                Yii::app()->end();
            }
            $this->redirect($url);
        }

        return $loginForm;
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->request->urlReferrer);
    }


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id = false)
	{
        if (!$id) $id = Yii::app()->user->id;
        if ($this->isUserAccessable($id)){
            $user = $this->loadModel($id);
            $this->pageTitle = "Редактирование <".$user->username.">";
            $user->scenario = 'update';

            if(isset($_POST['User'], $_POST['UserProfile']))
            {
                if ($user->saveRelated($_POST)){

                    if (Yii::app()->user->id == $id) {
                        Yii::app()->user->setState('fullName',$_POST['UserProfile']['FullName']);
                        Yii::app()->user->setState('inform_me',$_POST['UserProfile']['inform_me']);
                    }

                    if (Yii::app()->user->checkAccess('m_users')) {
                        $this->redirect(array('index'));
                    } else {
                        $this->redirect(array('update','id' => $user->id));
                    }
                }
            }

            $user['password'] = Yii::app()->params['emptyPassword'];
            $user['password_repeat'] = Yii::app()->params['emptyPassword'];

            $this->render('update',array(
                'model' => $user,
            ));
        } else {
            throw new CHttpException('403','Вы не можете работать с данными других пользователей.');
        }
	}

    private function isUserAccessable($id){
        if (!Yii::app()->user->checkAccess('m_users')) {
            if (Yii::app()->user->id !== $id) {
                return false;
            }
        }
        return true;
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id){

        $this->loadModel($id)->deleteUserForce();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

    public function actionMultiDelete(){
        $result = User::deleteUsers($_POST);
        $this->sendJson($result);
    }

    public function actionChangeRole(){
        $result = User::changeUsersRoles($_POST);
        $this->sendJson($result);
    }

    public function actionSubscribe($id, $subscribe){
        if ($this->isUserAccessable($id)){
            $user = $this->loadModel($id);
            $user->user_profile->inform_me = $subscribe;
            if ($user->user_profile->save()){
                Yii::app()->user->setState('inform_me',$subscribe);
            }
        }
        $this->redirect(Yii::app()->request->urlReferrer);
    }

    private function sendJson($result){
        header('Content-type: application/json');
        switch ($result) {
            case User::USER_OK:
                echo '{"status": "ok", "refresh": 1}';
                break;
            case User::EMPTY_USERS:
                echo '{"status": "ok", "refresh": 0, "error": "Вы забыли выбрать пользователей."}';
                break;
            case User::EMPTY_ROLE:
                echo '{"status": "ok", "refresh": 0, "error": "Вы забыли выбрать новую роль."}';
                break;
            case User::DELETE_ACTIVE_USER:
                echo '{"status": "ok", "refresh": 1, "error": "Нельзя удалять активных пользователей."}';
                break;
            default:
                echo '{"status": "ok", "refresh": 1,  "error": "Ошибка при выполнении операции. Пожалуйста повторите запрос позже."}';
        }
    }

	/**
	 * Lists all models.
	 */
	public function actionIndex(){
        $EmptyUser = New User();
        $EmptyUser->user_profile = new UserProfile();
        $EmptyUser->role = new Role();

        $this->pageTitle = "Все пользователи";

		$dataProvider=new CActiveDataProvider('User', array(
            'criteria'=>array(
                'with' => array(
                    'role'=>array(
                        'alias'=>'role'
                    ),
                    'user_profile'=>array(
                        'alias'=>'user_profile'
                    )
                )
            )
        ));

		$this->render('index',array(
			'dataProvider' => $dataProvider,
            'model' => $EmptyUser
		));
	}

    public function actionChangePassword($login_code){
        if (!Yii::app()->user->isGuest){
            Yii::app()->user->logout();
            $this->refresh();
        }


        $this->pageTitle = "Восстановление пароля";
        $url = Yii::app()->getBaseUrl(true);
        if ($login_code){
                $user = User::model()->findByAttributes(
                    array('login_code'=>$login_code)
                );
                if ($user){
                    if(isset($_POST['User']['password']))
                    {
                        $user->password = $_POST['User']['password'];
                        $user->password_repeat = $_POST['User']['password_repeat'];
                        if ($user->save()){
                            $user->eraseLoginCode();
                            $this->loginMe(array(
                                'username' => $user->username,
                                'password' => $_POST['User']['password'],
                            ), $url);

                        }
                    }

                    $user->password = '';
                    $user->password_repeat = '';

                    $this->render('changePassword',array(
                        'model' => $user
                    ));
                    return;
                }
        }
        $this->redirect($url);
    }

    public function actionRestorePassword(){
        header('Content-type: application/json');
        $response = array();

        $comment = new NewsComment();
        if(isset($_POST['username']))
        {
            $user = User::model()->findByAttributes(
                array('username' => $_POST['username'])
            );
            if ($user){
                if($user->createLoginCode()) {
                    $response['success'] = true;
                    $response['title'] = 'Письмо отправлено';
                    $response['text'] = 'На ваш адрес электронной почты отправлео письмо с дальнейшими инструкциями.';
                } else {
                    $response['success'] = false;
                    $response['title'] = 'Ошибка при восстановлении пароля';
                    $response['text'] = $comment->getErrors();
                }
            } else {
                $response['success'] = false;
                $response['text'] = 'Данный пользователь отсутствует в системе';
                $response['title'] = 'Пользователь не найден';
            }
        }
        echo CJSON::encode($response);
        Yii::app()->end();
    }



	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Запрошенная страница не найдена.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
