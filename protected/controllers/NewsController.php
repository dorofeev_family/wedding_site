<?php

class NewsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'getNewsComments'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('addcomment'),
				'roles'=>array('user'),
			),
            array('allow',
                'actions'=>array('create','update'),
                'roles'=>array('m_content')
            ),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'roles'=>array('admin'),
			),
            array('allow',
                'actions'=>array('editcomment','deletecomment'),
                'roles'=>array('m_comment')
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $news = $this->loadModel($id);

        if ($news->status_id != News::PUBLIC_NEWS_STATUS_ID){
            if (!Yii::app()->user->checkAccess('m_content')){
                $this->redirect(array('user/login'));
            }
        }

        $this->pageTitle = $news->title;
        $this->description = $news->description;
        $this->keywords = $news->keywords;
		$this->render('view',array(
			'model' => $news,
		));
	}

    public function actionGetNewsComments($id){
        echo $this->getCommentsAsHTML($id);
    }

    protected function getCommentsAsHTML($newsId){
        $news = $this->loadModel($newsId);
        $comment = new NewsComment();
        $totalCommentsCount = NewsComment::model()->countByAttributes(array('news_id' => $news->id));
        $allComments = NewsComment::GetAllNewsComments($news->id);

        $msgHTML = $this->renderPartial('_comments', array(
            'model' => $news,
            'totalCommentsCount' => $totalCommentsCount,
            'allComments' => $allComments,
            'fakeComment' => $comment
        ), true);

        return $msgHTML;
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        $this->layout = '';
		$model=new News;
        $model->author = new User();
        $model->status = new NewsStatus();

        $this->pageTitle = "Новая новость";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['News']))
		{
			$model->attributes=$_POST['News'];
            $model->deleteImageFlag = $_POST['deleteImageFlag'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->short_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $this->layout = '';
		$model = $this->loadModel($id);

        $this->pageTitle = "Редактирование <" . $model->title.">";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['News']))
		{
			$model->attributes=$_POST['News'];
            $model->deleteImageFlag = $_POST['deleteImageFlag'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->short_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $this->pageTitle = "Все новости";

        $dataProvider = News::getAllNews();
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

    public function actionAddComment(){
        header('Content-type: application/json');
        $response = array();

        $comment = new NewsComment();
        if(isset($_POST['NewsComment']))
        {
            $_POST['NewsComment']['news_id'] = $_POST['News']['id'];
            $_POST['NewsComment']['user_id'] = Yii::app()->user->getId();
            $comment->attributes = $_POST['NewsComment'];
            if($comment->save()) {
                $response['success'] = true;
                $response['text'] = 'Комментарий сохранен';
                $response['html'] = $this->getCommentsAsHTML($_POST['news_id']);
            } else {
                $response['success'] = false;
                $response['errors'] = $comment->getErrors();
                $response['text'] = 'Комментарий не сохранен';
            }
        }
        echo CJSON::encode($response);
        Yii::app()->end();
    }

    public function actionEditComment(){
        header('Content-type: application/json');
        $response = array();

        if(isset($_POST))
        {
            $comment = NewsComment::model()->findByPk($_POST['id']);
            $comment->text = $_POST['text'];
            if($comment->save()) {
                $response['success'] = true;
                $response['text'] = 'Комментарий сохранен';
                $response['html'] = $this->getCommentsAsHTML($_POST['news_id']);
            } else {
                $response['success'] = false;
                $response['errors'] = $comment->getErrors();
                $response['text'] = 'Комментарий не сохранен';
            }
        }
        echo CJSON::encode($response);
        Yii::app()->end();
    }

    public function actionDeleteComment(){
        header('Content-type: application/json');
        $response = array();

        if(isset($_POST))
        {
            $comment = NewsComment::model()->findByPk($_POST['id']);
            $countOfRelatedComments = NewsComment::model()->countByAttributes(array('parent_id' => $_POST['id']));
            if ($countOfRelatedComments == 0){
                try {
                    $comment->delete();
                    $response['success'] = true;
                    $response['text'] = 'Комментарий удален';
                    $response['html'] = $this->getCommentsAsHTML($_POST['news_id']);
                } catch (Exception $e) {
                    $response['success'] = false;
                    $response['error'] = "Возникла ошибка при удалении комментария.";
                    $response['text'] = 'Комментарий не удален';
                }
            } else {
                $response['success'] = false;
                $response['error'] = "Нельзя удалить комментарий, на который есть ответы.";
                $response['text'] = 'Комментарий не удален';
            }

        }
        echo CJSON::encode($response);
        Yii::app()->end();
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        $this->pageTitle = "Управление новостями";
		$model=new News('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['News']))
			$model->attributes=$_GET['News'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return News the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=News::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param News $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='news-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
