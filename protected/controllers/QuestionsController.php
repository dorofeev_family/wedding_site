<?php

class QuestionsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index', 'addQuestion', 'userQuestions', 'deleteQuestion', 'getQuestionByType'),
                'roles'=>array('c_question')
			),
            array('allow',
                'actions'=>array('admin', 'changeQuestionStatus', 'GetSingleQuestionInfo', 'EditQuestion'),
                'roles'=>array('m_question')
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $this->pageTitle = "Вопрос-Ответ";
		$dataProvider = Questions::getAllQuestions();
        $fakeQuestion = new Questions();
		$this->render('index',array(
            'fakeQuestion' => $fakeQuestion,
            'dataProvider'=>$dataProvider,
            'filter' => $this->getAllAvailableFilters()
		));
	}

    public function actionAddQuestion(){
        header('Content-type: application/json');

        $response = array();

        $question = new Questions();
        if(isset($_POST['Questions']))
        {
            $_POST['Questions']['user_id'] = Yii::app()->user->getId();
            $question->attributes = $_POST['Questions'];
            if($question->save()) {
                $response['success'] = true;
                $response['text'] = 'Вопрос отправлен!';
            } else {
                $response['success'] = false;
                $response['errors'] = $question->getErrors();
                $response['text'] = 'Вопрос не отправлен!';
            }
        }
        echo CJSON::encode($response);
        Yii::app()->end();
    }

    public function actionDeleteQuestion(){

        header('Content-type: application/json');
        $response = array();

        if(isset($_POST))
        {
            $question = Questions::model()->findByPk($_POST['id']);

            if (!$question) {
                $response['success'] = false;
                $response['error'] = "Вопрос не найден.";
                $response['text'] = 'Возможно вопрос уже удален.';
            } else {
                if (
                    $question->user_id == Yii::app()->user->getId() or
                    Yii::app()->user->checkAccess('m_question')
                ) {
                    try {
                        if ($question->answer){
                            $question->answer->delete();
                        }
                        $question->delete();
                        $response['success'] = true;
                        $response['text'] = 'Вопрос удален';
                    } catch (Exception $e) {
                        $response['success'] = false;
                        $response['error'] = "Возникла ошибка при удалении вопроса.";
                        $response['text'] = 'Вопрос не удален';
                    }
                } else {
                    $response['success'] = false;
                    $response['error'] = "Нельзя удалять чужие вопросы.";
                    $response['text'] = 'Вопрос не удален';
                }
            }
        }
        echo CJSON::encode($response);
        Yii::app()->end();
    }

    public function actionUserQuestions()
    {
        $this->pageTitle = "Мои вопросы";
        $dataProvider = Questions::getAllUserQuestions();
        $this->render('usersQuestions',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    private function getAllAvailableFilters(){
        $filter = array('all' => 'Все',
            Questions::NO_ANSWER => 'Без ответа',
            Questions::WITH_ANSWER => 'С ответами');

        $statuses = QuestionStatus::model()->findAll();
        $list = CHtml::listData($statuses,'id', 'name');


        $filter = $filter + $list;

        return $filter;
    }

    public function actionGetQuestionByType($type){
        echo $this->getQuestionAsHTML($type);
    }

    public function getQuestionAsHTML($type){
        $dataProvider = Questions::getAllQuestions($type);

        return $this->renderPartial('availableQuestions', array(
            'dataProvider'=>$dataProvider,
        ), true);
    }

    public function actionChangeQuestionStatus(){
        header('Content-type: application/json');
        $response = array();

        if(isset($_POST))
        {
            $question = Questions::model()->findByPk($_POST['id']);

            if (!$question) {
                $response['success'] = false;
                $response['error'] = "Вопрос не найден.";
                $response['text'] = 'Данные не изменены';
            } else {
                if (Yii::app()->user->checkAccess('m_question')) {

                    if ($_POST['approve']) {
                        $question->status_id = QuestionStatus::APPROVE_STATUS_ID;
                        $response['text'] = 'Вопрос опубликован';
                    } else {
                        $question->status_id = QuestionStatus::NEW_STATUS_ID;
                        $response['text'] = 'Вопрос отправлен в черновики';
                    }

                    if ($question->save()) {
                        $response['success'] = true;
                    } else {
                        $response['success'] = false;
                        $response['error'] = "Возникла ошибка при смене статуса.";
                        $response['text'] = 'Статус вопроса не изменен';
                    }
                } else {
                    $response['success'] = false;
                    $response['error'] = "Вы не можете менять статус вопросов.";
                    $response['text'] = 'Статус не изменен';
                }
            }
        }
        echo CJSON::encode($response);
        Yii::app()->end();
    }

    public function ActionGetSingleQuestionInfo(){
        header('Content-type: application/json');
        $response = array();

        if(isset($_POST))
        {
            $question = Questions::model()->findByPk($_POST['id']);

            if (!$question) {
                $response['success'] = false;
                $response['error'] = "Вопрос не найден.";
                $response['text'] = 'Данные не доступны.';
            } else {
                if (Yii::app()->user->checkAccess('m_question')) {
                    $response['success'] = true;
                    $response['question'] = array();
                    $response['question']['question'] = $question;
                    $response['question']['status'] = $question->status;
                    $response['question']['question_user'] = $question->user->user_profile;
                    $response['answer'] = array();
                    $response['answer']['answer'] = $question->answer;
                    if ($question->answer){
                        $response['answer']['answer_user'] = $question->answer->respondent->user_profile;
                    } else {
                        $response['answer']['answer_user'] = Yii::app()->user->fullName;
                    }

                    $response['all_statuses'] = QuestionStatus::model()->findAll();
                } else {
                    $response['success'] = false;
                    $response['error'] = "Вы не можете редактировать вопросы.";
                    $response['text'] = 'Редактирование не доступно';
                }
            }

        }
        echo CJSON::encode($response);
        Yii::app()->end();
    }

    public function actionEditQuestion(){
        header('Content-type: application/json');
        $response = array();

        if(isset($_POST))
        {
            $question = Questions::model()->findByPk($_POST['id']);

            if (!$question) {
                $response['success'] = false;
                $response['error'] = "Вопрос не найден.";
                $response['text'] = 'Данные не изменены.';
            } else {
                if (Yii::app()->user->checkAccess('m_question')) {

                    if ($question->answer){
                        $answer = $question->answer;
                        $answer->answer = $_POST['answer'];
                    } else {
                        $answer = new Answers();
                        $answer->answer = $_POST['answer'];
                        $answer->respondent_id = Yii::app()->user->id;
                    }

                    if ($answer->save()){
                        $response['answer'] = $answer;
                        $question->question = $_POST['question'];
                        $question->status_id = $_POST['status'];
                        $question->answer_id = $answer->id;

                        if ($question->save()) {
                            $response['success'] = true;
                            $response['text'] = 'Вопрос успешно изменен!';
                        } else {
                            $response['success'] = false;
                            $response['error'] = "Возникла ошибка при сохранении вопроса.";
                            $response['text'] = 'Вопрос не изменен';
                        }
                    } else {
                        $response['success'] = false;
                        $response['error'] = "Возникла ошибка при сохранении ответа на вопрос.";
                        $response['text'] = 'Вопрос не изменен';
                    }
                } else {
                    $response['success'] = false;
                    $response['error'] = "Вы не можете редактировать вопросы.";
                    $response['text'] = 'Редактирование не доступно';
                }
            }
        }
        echo CJSON::encode($response);
        Yii::app()->end();
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Questions the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Questions::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Questions $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='questions-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
