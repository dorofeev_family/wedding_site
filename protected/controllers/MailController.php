<?php

class MailController extends Controller
{
	// Uncomment the following methods and override them if needed

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('admin', 'sendMail'),
                'roles'=>array('m_email')
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionAdmin()
    {
        $this->pageTitle = "Массовая рассылка";
        $this->render('admin');
    }

    public function actionSendMail()
    {
        header('Content-type: application/json');
        $response = array();
        $mSender = new MailSender();
        if(isset($_POST['use_template_switch']))
        {
            if($mSender->sendTemplateMail($_POST['template'])) {
                $response['success'] = true;
                $response['text'] = 'Сообщения отправлены';
            } else {
                $response['success'] = false;
                $response['errors'] = 'Возникла ошибка при отправке сообщений';
                $response['text'] = 'Сообщения не отправлены';
            }
        } else {
            if (isset($_POST['text'])){
                $text = trim($_POST['text']);
                $text = trim($text, "<br/>");
                $text = trim($text, "&nbsp;");
                if ($text){
                    if($mSender->sendWritedMail($_POST['text'])) {
                        $response['success'] = true;
                        $response['text'] = 'Сообщения отправлены';
                    } else {
                        $response['success'] = false;
                        $response['errors'] = 'Возникла ошибка при отправке сообщений';
                        $response['text'] = 'Сообщения не отправлены';
                    }
                } else {
                    $response['success'] = false;
                    $response['errors'] = 'Сообщение не может быть пустым';
                    $response['text'] = 'Сообщения не отправлены';
                }

            } else {
                $response['success'] = false;
                $response['errors'] = 'Недостаточно параметров';
                $response['text'] = 'Сообщения не отправлены';
            }
        }

        echo CJSON::encode($response);
        Yii::app()->end();
    }
}