<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');

setlocale(LC_ALL, 'ru_RU.UTF-8');

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Семейный блог Дорофеевых',
    'language' => 'ru',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'ext.yii-mail.YiiMailMessage',
	),

    'theme'=>'bootstrap',
    'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
            'generatorPaths'=>array(
                'bootstrap.gii',
            ),
			'class'=>'system.gii.GiiModule',
			'password'=>'gii',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
            'class' => 'WebUser',
            'loginUrl'=>array('user/login'),
		),

        'bootstrap'=>array(
            'class'=>'bootstrap.components.Bootstrap',
        ),

		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName' => false,
			'rules'=>array(
				//'<controller:\w+>/<id:\d+>'=>'<controller>/view',
                'about' => 'site/about',
                'news/<action>/<id>'=>'news/<action>',
                'user/changePassword/<login_code>'=>'user/changePassword',
                'questions/GetQuestionByType/<type>'=>'questions/GetQuestionByType',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                '<controller:\w+>'=>'<controller>',

			),
		),

        'authManager' => array(
            // Будем использовать свой менеджер авторизации
            'class' => 'PhpAuthManager',
            // Роль по умолчанию. Все, кто не админы, модераторы и юзеры — гости.
            'defaultRoles' => array('guest'),
        ),

        'mail'=>(LOCAL) ? require(dirname(__FILE__).'/mail.php') : (require(dirname(__FILE__).'/prod_mail.php')),

		// database settings are configured in database.php
		'db'=>(LOCAL) ? require(dirname(__FILE__).'/database.php') : (require(dirname(__FILE__).'/prod_database.php')),

        'clientScript' => array(
            'class' => 'application.vendor.yii-EClientScript.EClientScript',
            'combineScriptFiles' => false, // By default this is set to true, set this to true if you'd like to combine the script files
            'combineCssFiles' => !YII_DEBUG, // By default this is set to true, set this to true if you'd like to combine the css files
            'optimizeScriptFiles' => false, // @since: 1.1
            'optimizeCssFiles' => false, // @since: 1.1
            'optimizeInlineScript' => false, // @since: 1.6, This may case response slower
            'optimizeInlineCss' => false, // @since: 1.6, This may case response slower
        ),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
/*
				array(
					'class'=>'CWebLogRoute',
				),
*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
    'params'=>(LOCAL) ? require(dirname(__FILE__).'/params.php') : (require(dirname(__FILE__).'/prod_params.php')),

);
