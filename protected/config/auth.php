<?php
return array(
    /*                  COMMENTS                 */
    'profile' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Доступ в личный кабинет',
        'bizRule' => null,
        'data' => null
    ),

    'edit_profile' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Редактирование профиля пользователя',
        'bizRule' => null,
        'data' => null
    ),

    'control_panel' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Доступ в панель управления',
        'bizRule' => null,
        'data' => null
    ),

    'm_users' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Управление пользователями',
        'bizRule' => null,
        'data' => null
    ),

    'r_content' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Доступ к данным',
        'bizRule' => null,
        'data' => null
    ),

    'm_content' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Управление контентом',
        'bizRule' => null,
        'data' => null
    ),

    'c_comment' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Оставлять комментарии',
        'bizRule' => null,
        'data' => null
    ),

    'm_comment' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Управление комментариями',
        'bizRule' => null,
        'data' => null
    ),

    'c_question' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Задавать вопросы',
        'bizRule' => null,
        'data' => null
    ),

    'm_question' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Управление вопросами',
        'bizRule' => null,
        'data' => null
    ),

    'm_email' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Массовая рассылка',
        'bizRule' => null,
        'data' => null
    ),


    /*                ROLES                 */

    'guest' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Гость',
        'bizRule' => null,
        'data' => null
    ),
    'notactive' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Гость',
        'bizRule' => null,
        'children' => array(
            'guest',
            'profile',
        ),
        'data' => null
    ),
    'user' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Пользователь',
        'children' => array(
            'notactive',
            'r_content',
            'c_comment',
            'c_question'
        ),
        'bizRule' => null,
        'data' => null
    ),
    'admin' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Администратор',
        'children' => array(
            'user',
            'control_panel',
            'm_users',
            'm_content',
            'm_comment',
            'm_question',
            'm_email'
        ),
        'bizRule' => null,
        'data' => null
    ),
);