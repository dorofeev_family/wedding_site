<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Пользователи',
);
?>

<h1>Все пользователи</h1>

<?php

echo CHtml::link('Создать пользователя',array('user/create'),array('class'=>'btn btn-sm'));

$form = $this->beginWidget('CActiveForm', array(
    'id'=>'user-form',
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
));

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $dataProvider,
    'enablePagination'=>true,
    'id'=>'users',
    'template'=>'{items} {pager}',
    'columns'=>array(
        array(
            'class'=>'CCheckBoxColumn',
            'id'=>'check_user',
            'selectableRows'=>2,
        ),
        array(
            'name' => 'id',
            'value' => 'CHtml::encode($data->id)',
            'type' => 'raw',
        ),
        array(
            'name'=>'username',
            'type'=>'raw',
            'value'=>'CHtml::link(CHtml::encode($data->username), array("update", "id"=>$data->id))',
        ),
        'user_profile.FullName',
        'user_profile.e_mail',
        array(
            'name' => 'role.role',
            'value' => '$data->role->name'
        ),
        array(            // display 'author.username' using an expression
            'name' => 'user_profile.inform_me_table',
            'value' => 'getInformName($data)',
        )
    ),
));

$roleOptions = CHtml::listData(Role::model()->findAll(), 'id', 'name');
echo $form->dropDownList($model->role, 'id', $roleOptions,
    array('empty' => '(Выберите новую роль)'));

echo CHtml::ajaxSubmitButton(
    'Изменить',
    '/user/changeRole',
    array(
        'type' => 'POST',
        'dataType' => 'json',
        'data' => 'js:
        {
            users : $.fn.yiiGridView.getChecked("users","check_user").toString(),
            role : $("#Role_id").val()
        }',
        'success' => 'function(response) {
            if (response.error) {
                alert(response.error);
            }
            if (response.refresh == 1) {
                $.fn.yiiGridView.update("users");
            }
        }',
        'error' => 'function(response){
            console.log(response);
        }'
    ),
    array(
        'type' => 'submit'
    ));

echo CHtml::ajaxSubmitButton(
    'Удалить',
    '/user/multiDelete',
    array(
        'type' => 'POST',
        'data'=>'js:
        {
            users : $.fn.yiiGridView.getChecked("users","check_user").toString(),
        }',
        'success' => 'function(response) {
            if (response.error) {
                alert(response.error);
            }
            if (response.refresh == 1) {
                $.fn.yiiGridView.update("users");
            }
        }',
        'beforeSend'=>'js:function() {
            var r = confirm("Вы уверены, что хотите удалить пользователей?");
            if(!r){
                return false;
            }
        }',
    ),
    array(
        'type' => 'submit'
    ));

$this->endWidget();


function getInformName($data){
    if ($data->user_profile->inform_me) {
        echo CHtml::encode($data->user_profile->getAttributeLabel('inform_me_true'));
    } else {
        echo CHtml::encode($data->user_profile->getAttributeLabel('inform_me_false'));
    }
}

?>