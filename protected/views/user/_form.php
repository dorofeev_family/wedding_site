<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',

    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

	<p class="note">Поля отмеченные <span class="required">*</span> не могут быть пустыми:</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'password_repeat'); ?>
        <?php echo $form->passwordField($model,'password_repeat',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'password_repeat'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model->user_profile,'FullName'); ?>
        <?php echo $form->textField($model->user_profile,'FullName',array('size'=>60,'maxlength'=>256)); ?>
        <?php echo $form->error($model->user_profile,'FullName'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model->user_profile,'e_mail'); ?>
		<?php echo $form->textField($model->user_profile,'e_mail',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model->user_profile,'e_mail'); ?>
	</div>

    <div class="row">
        <?=$form->labelEx($model->user_profile,'inform_me')?>
        <label class="user-switch switch">
            <?php echo $form->checkBox($model->user_profile,'inform_me',  array('class'=>'user-switch switch-input')); ?>
            <span class="switch-label" data-on="Да" data-off="Нет"></span>
            <span class="switch-handle"></span>
        </label>
    </div>


    <?php if (Yii::app()->user->checkAccess('m_users')) {?>
        <div class="row">
            <?php echo $form->labelEx($model,'role_id'); ?>
            <?php $roleOptions = CHtml::listData(Role::model()->findAll(), 'id', 'name'); ?>
            <?php echo $form->dropDownList($model, 'role_id', $roleOptions); ?>
            <?php echo $form->error($model,'role_id'); ?>
        </div>
    <?php } ?>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
            array(
                'class' => 'site_button',
            )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->