<?php
/* @var $this UserController */
/* @var $model User */

$this->pageTitle=Yii::app()->name . ' - Регистрация';
?>

<h1 class="by-center">Регистрация</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>