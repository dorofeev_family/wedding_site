<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
    'Пользователи'=>array('index'),
    'Новый',
);

?>

<h1>Создать нового пользователя</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>