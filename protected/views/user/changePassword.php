<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
    $model->username,
);

?>

<h1>Восстановление пароля</h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

	<p class="note">Поля отмеченные <span class="required">*</span> не могут быть пустыми:</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'password_repeat'); ?>
        <?php echo $form->passwordField($model,'password_repeat',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'password_repeat'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Сохранить',
            array(
                'class' => 'site_button',
            )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->