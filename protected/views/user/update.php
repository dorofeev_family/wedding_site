<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	$model->username,
);

?>

<h1>Данные пользователя: <b><i><?php echo $model->username; ?></i></b></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>