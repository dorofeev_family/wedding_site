<?php
if ($usePagination){
    $template = "{items}\n{pager}";
} else {
    $template = '{items}';
}
?>

<div class="news_preview_list">
<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'template' => $template,
    'itemView'=>'/news/_newsPreview',
    'afterAjaxUpdate'=>'init2GIS',
    //'separator' => '<hr class="news-spliter"/>',
    'htmlOptions' => array(
        'class' => 'newsColumn'
    ),
    'pager' => array(
        'class' => 'CLinkPager',
        'header' => '',
        'htmlOptions' => array(
            'class' => 'news-pager'
        )
    ),
)); ?>
</div>