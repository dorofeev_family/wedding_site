<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'Все новости'=>array('index'),
	'Новая',
);
?>

<h1>Новая новость</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>