<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'Все новости'=>array('index'),
	'Управление новостями',
);

?>

<h1>Управление новостями</h1>
<?php echo CHtml::link('Создать новость',array('news/create'),array('class'=>'btn btn-sm')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'news-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
        array(
            'name' => 'id',
            'value' => '$data->short_id'
        ),
		'title',
		'createdate',
        array(
            'name' => 'author_id',
            'value' => '$data->author->username'
        ),
        array(
            'name' => 'status_id',
            'value' => '$data->status->name'
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
