<?php
/* @var $this NewsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = false;

$this->fullsize = true;
$empty = true;
?>

    <hr class="news-spliter"/>
    <h1 class="by-center">Все новости</h1>
    <hr class="news-spliter"/>


<?php if ($dataProvider->itemCount) { ?>
    <?php $this->renderPartial('/news/availablenews', array(
        'dataProvider' => $dataProvider,
        'usePagination' => true
    )); ?>
    <?php $empty = false; ?>
<?php }?>

<?php if ($empty) {?>
    <h2>Ой-ой-ой, Новостей нет!</h2>
    <p>Все новости куда-то пропали! Возможно идут какие-то работы на сайте. Зайдите попозже ;)</p>
<?php }?>