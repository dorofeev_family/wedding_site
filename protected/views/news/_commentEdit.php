<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'edit-comment-modal')); ?>
    <div class="modal-header">

        <h3>Редактирование комментария</h3>
    </div>
    <div class="modal-body">
        <form id="edit-comment-form">
            <div id="comment-form-box" class="comment-form">
                <div class="comment_user"><b>Пользователь</b></div>
                <div class="comment_date">00-00-0000 00:00:00</div>
                <div class="comment_id">5</div>
                <textarea id="comment_text" name="text" class='comment-text'></textarea>
                <div class="parent-info-div"></div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Сохранить',
            'type'=>'success',
            'htmlOptions'=>array('OnClick'=>'sendEditCommentRequest()'),
        )); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Отмена',
            'type'=>'danger',
            'htmlOptions'=>array('data-dismiss'=>'modal'),
        )); ?>
    </div>
<?php $this->endWidget(); ?>