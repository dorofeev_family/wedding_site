<?php
/* @var $this NewsController */
/* @var $data News */
?>

<div id="<?=$data->short_id?>" class="single_news preview">
    <div id="news-data">
        <news class="front">
            <div class="front_news_title_box">
                <div class="front_news_title">
                    <h2><?php echo CHtml::link(CHtml::encode($data->title), array('/news/view', 'id'=>$data->short_id)); ?></h2>
                </div>
            </div>
            <div class="news-main-image">
            <?php if($data->image){ ?>
                <?php echo CHtml::image('/images/news/'.$data->image,"",array(
                    "id" => 'newsMainImage',
                )); ?>
            <?php } else { ?>
                    <img id="newsMainImage" src="/images/no-news-image.png"/>
            <?php } ?>
            </div>
        </news>
        <news class="side"></news>
        <news class="back">
            <?php if (!isset($update)){?>
                <div class="news_title_box">
                    <div class="news_title">
                        <h2><?php echo CHtml::link(CHtml::encode($data->title), array('/news/view', 'id'=>$data->short_id)); ?></h2>
                    </div>
                    <?php if (Yii::app()->user->checkAccess('m_content')) {?>
                        <div class="news_status">
                            <?php echo CHtml::link(CHtml::encode($data->status->name), array('/news/update', 'id'=>$data->short_id)); ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <div class="news_title_box">
                    <div class="news_title">
                        <h2><?php echo CHtml::link(CHtml::encode($data->title), array('/news/view', 'id'=>$data->short_id)); ?></h2>
                    </div>
                </div>
            <?php } ?>

            <div class="news_preview">
                <?= ($data->preview)?($data->preview):($data->text)?>
            </div>

            <?php if (!isset($update)){?>
                <div class="news_footer_box">
                    <div class="news_read_more">
                        <?= CHtml::link(CHtml::encode('Подробнее'), array('/news/view', 'id'=>$data->short_id), array(
                            'class' => 'btn btn-info'
                        )); ?>
                    </div>

                    <?php if (Yii::app()->user->checkAccess('m_content')) { ?>
                        <div class="news_date">
                            <?=CHtml::encode($data->createdate)?>
                        </div>

                        <div class="news_comment_count">
                            (<?=NewsComment::model()->countByAttributes(array('news_id'=>$data->id)) ?>)
                        </div>

                        <div class="news_author">
                            <?php echo CHtml::encode($data->author->user_profile->FullName); ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </news>

    </div>
</div>