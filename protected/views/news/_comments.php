<h3>Комментарии <div class="comment-count">(<?=$totalCommentsCount?>)</div></h3>
<?php
    if (Yii::app()->user->checkAccess('c_comment')) {

        function createComment($comment){
            echo '<div class="comment-box">';
            echo '<div class="comment-info" id="comment-id-'.$comment['id'].'" onclick="saveAsParent(this,'.$comment['id'].')">';
            echo '<div class="comment_user"><b>'.$comment['user'].'</b></div>';
            echo '<div class="comment_date">'.$comment['date'].'</div>';
            echo '<div class="comment_text">';
            echo '<p>'.nl2br($comment['text']).'</p>';
            echo '</div>';
            if (Yii::app()->user->checkAccess('m_comment')){
                echo '<div class="comment-manager-box">';
                echo '<a class="comment-manager-link" onClick="editComment('.$comment['id'].', event)">Редактировать</a>';
                echo '<a class="comment-manager-link" onClick="deleteComment('.$comment['id'].', event)">Удалить</a>';
                echo '</div>';
            }
            echo '</div>';
            if ($comment['children']){
                foreach ($comment['children'] as $child){
                    createComment($child);
                }
            }
            echo '</div>';
        }

        foreach ($allComments as $comment){
            createComment($comment);
        }


        ?>

        <div id="comment-form-box" class="comment-form">
            <div class="comment_user"><b><?=Yii::app()->user->fullName?></b></div>
            <div class="comment_date"><?php
                echo strftime('%d %b %Y', time());
                ?>
            </div>
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id'=>'comment-form',
                'enableClientValidation' => false,
                'htmlOptions'=>array(
                    'onsubmit'=>"return false;"
                )
            ));

            echo $form->textField($model,"id",array('class'=>"comment-news-id"));
            echo $form->textField($fakeComment,"parent_id",array('class'=>"comment-parent-id"));
            echo $form->textArea($fakeComment,'text',array('class'=>'comment-text'));
            ?>

            <?php
            echo CHtml::submitButton('Отправить',
                array(
                    'class' => 'site_button',
                    'id' => 'addcomment',
                    'onClick' => 'sendComment()'
                )
            );?>
            <div class="parent-info-div" id="parent-info"></div>
            <?php
            $this->endWidget();
            ?>
        </div>

        <?php $this->renderPartial('/news/_commentEdit'); ?>

<?php } else {
    echo "Чтобы увидеть комментарии пользователей, необходимо авторизоваться.";
} ?>