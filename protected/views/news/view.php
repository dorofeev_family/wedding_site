

<?php
/* @var $this NewsController */
/* @var $model News */

if($model->image) {
    $this->image = '/images/news/'.$model->image;
}

$this->breadcrumbs=array(
	'Новости'=>array('index'),
	$model->title,
);

Yii::app()->clientScript->registerScriptFile('/js/comments.js',CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile('/js/social_website.js',CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile('/js/newsImageZoom.js',CClientScript::POS_HEAD);

?>
<div class="news-full-data">

    <div class="single_news">

        <div class="news_title_box">
            <div class="news_title">
                <h2><?=CHtml::encode($model->title)?></h2>
            </div>
            <?php if (Yii::app()->user->checkAccess('m_content')) {?>
                <div class="news_status">
                    <?php echo CHtml::link(CHtml::encode($model->status->name), array('/news/update', 'id'=>$model->short_id)); ?>
                </div>
            <?php } ?>
        </div>

        <div class="news_preview">
            <?=$model->text?>
        </div>

        <div class="news_footer_box">
            <div class="news_share">
                <a onclick="sendwallpost('<?=Yii::app()->createUrl('news/view', array('id'=>$model->short_id))?>');" class="btn btn-info">Рассказать в VK</a>
            </div>

            <?php if (Yii::app()->user->checkAccess('m_content')) {?>
                <div class="news_date">
                    <?php echo CHtml::encode($model->createdate); ?>
                </div>

                <div class="news_author">
                    <?php echo CHtml::encode($model->author->user_profile->FullName); ?>
                </div>
            <?php } ?>

        </div>

        <hr class="news-spliter comment-padding"/>
        <script>
            var news_id = "<?=$model->short_id?>";
        </script>
        <div id="outer-comment-box"></div>
    </div>
</div>

<?php $this->renderPartial('/news/_showImage'); ?>