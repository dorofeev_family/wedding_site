<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>

<?php Yii::app()->clientScript->registerScriptFile("/js/translitiration.js", CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile("/js/imagePreview.js", CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile("/js/newsPreview.js", CClientScript::POS_END); ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Поля отмеченные <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
        <h3><?=$form->labelEx($model,'title')?></h3>
		<?php echo $form->textField($model,'title',array('maxlength'=>512, 'placeholder'=>'Название новости', 'class'=>'form-input-field', 'id'=>'news-title', 'onKeyup'=>'js:translit(this, "short_id")')); ?>
        <?php echo $form->error($model,'title'); ?>

	</div>

    <div class="row">
        <h3><?=$form->labelEx($model,'short_id')?></h3>
        <?php echo $form->textField($model,'short_id',array('maxlength'=>128, 'placeholder'=>'Уникальный идентификатор новости', 'class'=>'form-input-field', 'id'=>'short_id')); ?>
        <?php echo $form->error($model,'short_id'); ?>

    </div>

    <div class="row">
        <h3><?=$form->labelEx($model,'description')?></h3>
        <?php echo $form->textField($model,'description',array('maxlength'=>512, 'placeholder'=>'Краткое описание новости', 'class'=>'form-input-field', 'id'=>'description')); ?>
        <?php echo $form->error($model,'description'); ?>
    </div>

    <div class="row">
        <h3><?=$form->labelEx($model,'keywords')?></h3>
        <?php echo $form->textField($model,'keywords',array('maxlength'=>512, 'placeholder'=>'Ключевые слова через запятую', 'class'=>'form-input-field', 'id'=>'keywords')); ?>
        <?php echo $form->error($model,'keywords'); ?>
    </div>


    <div class="row">
        <h3><?=$form->labelEx($model,'show_on_main')?></h3>
        <label class="switch">
            <?php echo $form->checkBox($model,'show_on_main',  array('class'=>'switch-input')); ?>
            <span class="switch-label" data-on="Закрепить" data-off="Открепить"></span>
            <span class="switch-handle"></span>
        </label>
    </div>

    <div class="row">
        <div class="image-preview-row">
            <h3><?=$form->labelEx($model,'image')?></h3>
            <?php echo CHtml::button('Удалить',
                array(
                    'onclick' => 'deleteImage()',
                    'id' => 'delete-image-button'
                )); ?>
        </div>

        <div class="news-preview-on-update">
                <?php $this->renderPartial('/news/_newsPreview', array(
                    'data' => $model,
                    'update' => true
                )); ?>
        </div>


        <input type="text" name="deleteImageFlag" id="deleteImageFlagInput">
        <?php echo CHtml::activeFileField($model, 'image',
            array(
                'onchange' => 'uploadImage(this)',
                'data-target' => '#image-preview'
            )); ?>

        <?php echo $form->error($model,'image'); ?>
    </div>

	<div class="row">
		<h3><?=$form->labelEx($model,'preview')?></h3>
        <?php

        $toolbar = array(
            array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-','Undo', 'Redo'),
            array('Link', 'Unlink'),
            array('Image', 'Table', 'HorizontalRule', 'SpecialChar'),
            array('Source'),
            '/',
            array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
            array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'),
            array('Styles', 'Format', 'Font', 'FontSize'),
        );

        $this->widget('application.extensions.ckeditor.ECKEditor', array(
            'model'=>$model,
            'attribute'=>'preview',
            'language'=>'ru',
            'height'=>'120px',
            'editorTemplate'=>'advanced',
            'toolbar'=>$toolbar,
            'htmlOptions'=>array(
                'id'=>'preview-textarea',
            ),
        )); ?>
		<?php echo $form->error($model,'preview'); ?>
	</div>

    <br/>
    <br/>
	<div class="row">
		<h3><?=$form->labelEx($model,'text')?></h3>
        <?php $this->widget('application.extensions.ckeditor.ECKEditor', array(
            'model'=>$model,
            'attribute'=>'text',
            'language'=>'ru',
            'editorTemplate'=>'advanced',
            'toolbar'=>$toolbar
        )); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>

    <div class="row">
        <h3><?=$form->labelEx($model,'sort')?></h3>
        <div class="outer-slider-box">
            <div class="inner-slider-box">
                <?php $this->widget('zii.widgets.jui.CJuiSliderInput', array(
                    'model'=>$model,
                    'attribute'=>'sort',
                    'event'=>'change',
                    'id' => 'sortField_scroll',

                    // additional javascript options for the slider plugin
                    'options'=>array(
                        'min'=>1,
                        'max'=>200,
                        'animate'=>true,
                        'slide'=>'js:function(event,ui){$("#sortField").val(ui.value);}',
                    ),
                ));?>
            </div>
            <div class="left-slider-label">Начало<br/>страницы</div>
            <div class="middle-slider-label">
                <input type="text" id="sortField" value="<?=($model->sort)?($model->sort):100?>" onkeyup="$('#sortField_scroll_slider').slider('value', $(this).val());"/>
            </div>
            <div class="right-slider-label">Конец<br/>страницы</div>
        </div>


        <?php echo $form->error($model,'sort'); ?>
    </div>
    <?php if (Yii::app()->user->checkAccess('m_content')) { ?>
        <div class="row">
            <h3><?=$form->labelEx($model,'createdate')?></h3>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'model'=>$model, //Model object
                'attribute'=>'createdate', //attribute name
                'language' => 'ru',
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=>'yy-mm-dd',

                ), // jquery plugin options
                'htmlOptions'=>array(
                    'value' => date('Y-m-d')
                )
            ));
            ?>
            <?php echo $form->error($model,'createdate'); ?>
        </div>

        <div class="row">
            <h3><?=$form->labelEx($model,'author_id')?></h3>
            <?php $usersList = CHtml::listData(User::model()->with('role', 'user_profile')->findAll('role.role = "'.User::ROLE_ADMIN.'"'), 'id', 'user_profile.FullName'); ?>
            <?php
                $author = Yii::app()->user->id;
                if ($model->author_id){
                    $author = $model->author_id;
                }
            ?>
            <?=$form->dropDownList($model, 'author_id', $usersList,  array(
                'options' => array(
                    $author => array('selected'=>true)
                )))?>
            <?php echo $form->error($model,'author_id'); ?>
        </div>

        <div class="row">
            <h3><?=$form->labelEx($model,'status_id')?></h3>
            <?php $statusOptions = CHtml::listData(NewsStatus::model()->findAll(), 'id', 'name'); ?>
            <?=$form->dropDownList($model, 'status_id', $statusOptions)?>
            <?=$form->error($model,'status_id')?>
        </div>

    <?php } ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
            array(
                'class' => 'site_button',
            )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->