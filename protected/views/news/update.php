<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'Все новости'=>array('index'),
	'"'.$model->title.'"'=>array('view','id'=>$model->short_id),
    'Редактирование'
);
?>

<h1>Редкатирование новости: <i>"<?php echo $model->title; ?></i>"</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>