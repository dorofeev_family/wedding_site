<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="language" content="ru">

    <?php if ($this->description) { ?>
        <meta name="Description" content="Семейный блог Дорофеевых. <?=$this->description?>">
        <meta property="og:description" content="<?=$this->description?>"/>
    <?php } else { ?>
        <meta name="Description" content="Семейный блог Дорофеевых. Наши новости, истории, мысли и важные события.">
        <meta property="og:description" content="Семейный блог Дорофеевых. Наши новости, истории, мысли и важные события."/>
    <?php } ?>

    <?php if ($this->keywords) { ?>
        <meta name="Keywords" content="Дорофеевы, семейный блог, <?=$this->keywords?>">
    <?php } else { ?>
        <meta name="Keywords" content="Дорофеевы, Дорофеев Дмитрий, Дорофеева Евгения, Дима и Женя, семейный блог, Yii, истории, новости, фотографии">
    <?php } ?>

    <?php if ($this->image) { ?>
        <meta property="og:image" content="<?=Yii::app()->request->getBaseUrl(true). $this->image . "?rnd=".rand(0,1000)?>"/>
    <?php } ?>

    <meta property="og:title" content="<?=CHtml::encode($this->pageTitle)?>" />
    <meta property="og:site_name" content="Семейный блог" />

    <meta name=robots content="index, follow">

	<!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->
    <?php echo Yii::app()->bootstrap->init();?>

    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/form.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/main.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/news.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/switch.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/pager.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/comments.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/questions.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/mailSender.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/vendor/sweetalert-master/dist/sweetalert.css'); ?>


    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/adaptive/news-transform.css', '(min-width: 1025px)'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/adaptive/small-widescreen.css', '(max-width: 1500px)'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/adaptive/medium-width.css', '(max-width: 1300px)'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/adaptive/laptop.css', '(max-width: 1024px)'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/adaptive/mobile.css', '(max-width: 600px)'); ?>



    <?php Yii::app()->clientScript->registerCssFile('http://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic-ext&n5ketp', 'all'); ?>

    <script src="http://maps.api.2gis.ru/2.0/loader.js?pkg=full&lazy=true" data-id="dgLoader"></script>
    <script src="http://vk.com/js/api/openapi.js" type="text/javascript"></script>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link rel="shortcut icon" href="/images/favicon.ico">
</head>

<body>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-99030733-1', 'auto');
    ga('send', 'pageview');
</script>

<?php
    Yii::app()->clientScript->registerScriptFile("/js/jquery.plugin.min.js", CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile("/js/jquery.countdown.min.js", CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile("/js/onLoad.js", CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScriptFile('/vendor/sweetalert-master/dist/sweetalert.min.js');
?>

<div class="container" id="page">
    <div id="bg-image"></div>
    <?php
    $this->widget('bootstrap.widgets.TbNavbar', array(
        'brand'=>'Главная',
        'collapse'=>true,
        'brandUrl'=>'/',
        'htmlOptions'=>array('class' => 'main_menu', 'id' => 'main_menu'),
        'items' => array(
            array(
                'class' => 'bootstrap.widgets.TbMenu',
                'htmlOptions'=>array('class'=>'pull-left main_elements'),
                'items' => array(
                    //array('label'=>'Главная', 'url'=>array('/site/index')),
                    array('label'=>'Новости', 'url'=>array('/news/index')),
                    array('label'=>'Вопрос-ответ', 'url'=>array('/questions/index'), 'visible'=>Yii::app()->user->checkAccess('c_question')),
                    array('label'=>'О нас', 'url'=>array('/site/about')),
                )
            ),
            array(
                'class'=>'bootstrap.widgets.TbMenu',
                'htmlOptions'=>array('class'=>'pull-right additional_elements'),
                'items'=>array(

                    array(
                        'label'=>'Профиль',
                        'visible'=>!Yii::app()->user->isGuest,
                        'items'=>array(
                            array('label'=>'Мои данные', 'url'=>array('user/update','id'=>Yii::app()->user->id),),
                            array('label'=>'Мои вопросы', 'url'=>array('questions/userQuestions')),
                            array('label'=>'Отписаться от новостей', 'visible'=>Yii::app()->user->isSubscribed(), 'url'=>array('user/subscribe','id'=>Yii::app()->user->id, 'subscribe'=>0)),
                            array('label'=>'Подписаться на новости', 'visible'=>!Yii::app()->user->isSubscribed(), 'url'=>array('user/subscribe','id'=>Yii::app()->user->id, 'subscribe'=>1)),
                        )),
                    array(
                        'label'=>'Администрирование',
                        'items'=>array(
                            array('label'=>'Пользователи', 'visible'=>Yii::app()->user->checkAccess('m_users'), 'url'=>array('/user/index')),
                            array('label'=>'Новости', 'visible'=>Yii::app()->user->checkAccess('m_content'), 'url'=>array('/news/admin')),
                            array('label'=>'Массовая рассылка', 'visible'=>Yii::app()->user->checkAccess('m_email'), 'url'=>array('/mail/admin')),
                        )),
                    array('label'=>'Войти', 'visible'=>Yii::app()->user->isGuest, 'url'=>array('/user/login')),
                    array('label'=>'Регистрация', 'visible'=>Yii::app()->user->isGuest, 'url'=>array('/user/signin')),
                    array('label'=>'Выйти', 'visible'=>!Yii::app()->user->isGuest, 'url'=>array('user/logout')),
                ),
            ),
        )
    ));
    ?>

    <?php
    $controller = Yii::app()->getController();
    $isHome = $controller->getId() === 'site' && $controller->getAction()->getId() === 'index';
    if($isHome){
        $this->widget('CarouselWidget');
        $this->widget('CountDownWidget');
    } else {
        if (isset($this->image) and $this->image){
    ?>
            <div class="page-image-box">
                <?php
                 echo CHtml::image($this->image,"",array(
                    "id" => 'page-image',
                ));
                ?>
            </div>
    <?php
            $this->widget('CountDownWidget');
        }
    }
    ?>



    <div class="main-content-box">
        <div class="content-box <?=($this->fullsize)?(''):('limit-content-width')?>">
            <?php if(isset($this->breadcrumbs)):?>
                <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links'=>$this->breadcrumbs,
                    'homeLink'=>CHtml::link('Главная', array('site/index')),
                )); ?><!-- breadcrumbs -->
            <?php endif?>

            <?php echo $content; ?>
        </div>
    </div>

    <?php $this->renderPartial('/site/footer'); ?>


</div><!-- page -->

</body>
</html>
