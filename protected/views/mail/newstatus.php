<html>
<head>
</head>
<body>
Здравствуйте, <?php echo $user->user_profile->FullName; ?>
<br>Вы получили это письмо, т.к. ваш статус на сайте <?= CHtml::link(Yii::app()->name, Yii::app()->getBaseUrl(true))?> был изменен:
<br>
<br><b>Прежний статус</b> : <?php echo $old_status; ?>
<br><b>Новый статус</b> : <?php echo $new_status; ?>
<br>
<br> Мы вас ждём на сайте ;)
</body>
</html>