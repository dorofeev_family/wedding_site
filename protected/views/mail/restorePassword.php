<html>
<head>
</head>
<body>
Здравствуйте, <?php echo $user->user_profile->FullName; ?>
<p>Вы получили это письмо, т.к. на сайте <?= CHtml::link(Yii::app()->name, Yii::app()->getBaseUrl(true))?> был выполнен запрос на восстановление пароля.</p>
<p>Для того, чтобы ввести новый пароль, пройдите по ссылке:</p>
<ul>
    <li><?=CHtml::link(CHtml::encode("Восстановление пароля"), $this->createAbsoluteUrl("/user/changePassword/".$user->login_code))?></li>
</ul>
<p>Если вы не запрашивали смену пароля, просто проигнорируйте это письмо.</p>
</body>
</html>