<html>
<head>
</head>
<body>
Здравствуйте, <?php echo $user->user_profile->FullName; ?>
<br>Вы получили это письмо, т.к. кто-то изменил ваши данные на сайте <?= CHtml::link(Yii::app()->name, Yii::app()->getBaseUrl(true))?>.
<br>Ваши текущие данные:
<br>
<br><b>Логин</b> : <?php echo $user->username; ?>
<?php if ($user->password) { ?>
    <br><b>Пароль</b> : <?php echo $user->password_raw; ?>
<?php }?>
<br><b>ФИО</b> : <?php echo $user->user_profile->FullName; ?>
<br><b>e-mail</b> : <?php echo $user->user_profile->e_mail; ?>
<br><b>Группа</b> : <?php echo $user->role->name; ?>
<br>
<br>
<br>Если вы не изменяли свои данные, пожалуйста сообщите администрации сайта об этом и смените свой пароль.
</body>
</html>