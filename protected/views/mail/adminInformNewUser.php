<html>
<head>
</head>
<body>
Здравствуйте, <?php echo $admin->user_profile->FullName; ?>
<br>Вы получили это письмо, т.к. вы являетесь администратором сайта <?= CHtml::link(Yii::app()->name, Yii::app()->getBaseUrl(true))?>.
<br>На сайте был зарегистрирован новый пользователь:
<br>
<br><b>Логин</b> : <?php echo $user->username; ?>
<br><b>ФИО</b> : <?php echo $user->user_profile->FullName; ?>
<br><b>e-mail</b> : <?php echo $user->user_profile->e_mail; ?>
<br><b>Группа</b> : <?php echo $user->role->name; ?>
<br>
<br>
Одобрить или удалить пользователя вы можете на странице: <?= CHtml::link("Профиль пользователя", $this->createAbsoluteUrl("/user/update/",array('id'=>$user->id)))?>
</body>
</html>