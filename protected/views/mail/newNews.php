<html>
<head>
</head>
<body>
Здравствуйте, <?php echo $user->user_profile->FullName; ?>
<br>Вы получили это письмо, т.к. вы подписаны на обновления сайта <?= CHtml::link(Yii::app()->name, Yii::app()->getBaseUrl(true))?>.
<br>На сайт была добавлена новость:
<br>
<ul>
    <li><?=CHtml::link(CHtml::encode($news->title), $this->createAbsoluteUrl("/news/view/".$news->short_id))?></li>
</ul>
<p>Если вы не хотите больше получать уведомления с сайта - пожалуйста отключите их в настройках своего профиля на <?= CHtml::link("сайте", $this->createAbsoluteUrl("/user/update/",array('id'=>$user->id)))?>.</p>
</body>
</html>