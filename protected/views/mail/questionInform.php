<html>
<head>
</head>
<body>
Здравствуйте, <?php echo $user->user_profile->FullName; ?>
<br>Вы получили это письмо, т.к. вы подписаны на обновления сайта <?= CHtml::link(Yii::app()->name, Yii::app()->getBaseUrl(true))?>.
<br>Был получен ответ на ваш вопрос:
<br>
<hr/>
    <h3>Вопрос:</h3>
    <p><?=CHtml::encode($question->question)?></p>
<hr/>
    <h3>Ответ:</h3>
    <p><?=CHtml::encode($question->answer->answer)?></p>
<hr/>
<br>
Если вы не хотите больше получать уведомления с сайта - пожалуйста отключите их в настройках своего профиля на <?= CHtml::link("сайте", $this->createAbsoluteUrl("/user/update/",array('id'=>$user->id)))?>.
</body>
</html>