<?php
/* @var $this MailController */

$this->breadcrumbs=array(
    'Массовая рассылка'
);

Yii::app()->clientScript->registerScriptFile("/js/emailSender.js", CClientScript::POS_HEAD);

?>
<h1>Массовая рассылка</h1>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'email-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array(
            'onsubmit'=>"return false;"
        )
    )); ?>

    <div id="use-template-switch-box" class="row">
        <h3 >Использовать шаблон</h3>
        <label class="user-switch switch">

            <?php echo CHtml::CheckBox('use_template_switch',true, array (
                'value'=>'on',
                'class'=>'user-switch switch-input'
            )); ?>
            <span class="switch-label" data-on="Да" data-off="Нет"></span>
            <span class="switch-handle"></span>
        </label>
    </div>
    <div id="use_template">
            Выберите шаблон
            <?php echo CHtml::dropDownList('template', null,
                MailSender::$templates);
            ?>
    </div>
    <div id="own_text">
            <?php

            $toolbar = array(
                array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-','Undo', 'Redo'),
                array('Link', 'Unlink'),
                array('Image', 'Table', 'HorizontalRule', 'SpecialChar'),
                array('Source'),
                '/',
                array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
                array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'),
                array('Styles', 'Format', 'Font', 'FontSize'),
            );

            $this->widget('application.extensions.ckeditor.ECKEditor', array(
                'name'=>'text',
                'language'=>'ru',
                'height'=>'120px',
                'editorTemplate'=>'advanced',
                'toolbar'=>$toolbar
            )); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Отправить',
            array(
                'class' => 'site_button',
                'id' => 'email-sender-button',
                'onClick' => 'sendEmail()'
            )); ?>
    </div>

<?php $this->endWidget(); ?>
</div>