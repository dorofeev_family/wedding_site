<html>
<head>
</head>
<body>
Здравствуйте, <?php echo $user->user_profile->FullName; ?>
<br>Вы получили это письмо, т.к. были зарегистрированы на сайте <?= CHtml::link(Yii::app()->name, Yii::app()->getBaseUrl(true))?>.
<br>Для входа на сайт используйте следующие данные:
<br>
<br><b>Логин</b> : <?php echo $user->username; ?>
<br><b>Пароль</b> : <?php echo $user->password_raw; ?>
<br>
<br>
<?php if ($user->role->role = "notactive" ) { ?>
Как только администраторы сайта убедятся в том, что вы не робот вы получите доступ к контенту сайта.
<?php } else { ?>
Вы можете уже сейчас во всю использовать сайт ;)
<?php }?>
</body>
</html>