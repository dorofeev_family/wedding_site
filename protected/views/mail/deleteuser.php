<html>
<head>
</head>
<body>
Здравствуйте, <?php echo $user->user_profile->FullName; ?>
<br>Вы получили это письмо, т.к. ваш аккаунт на сайте <?= CHtml::link(Yii::app()->name, Yii::app()->getBaseUrl(true))?> был удален:
<br>
<br>Если вы считаете, что ваш аккаунт удален по ошибке, обратитесь по адресу: <?php echo Yii::app()->params['adminEmail'];?>
<br>
<br>
</body>
</html>