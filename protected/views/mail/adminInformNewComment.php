<html>
<head>
</head>
<body>
Здравствуйте, <?php echo $admin->user_profile->FullName; ?>
<br>Вы получили это письмо, т.к. вы являетесь администратором сайта <?= CHtml::link(Yii::app()->name, Yii::app()->getBaseUrl(true))?>.
<br>На сайте был добавлен новый комментарий пользователем <b><?=$comment->user->user_profile->FullName?></b>,
к новости <?= CHtml::link($comment->news->title, $this->createAbsoluteUrl("/news/view/".$comment->news->short_id))?>
<br>
<hr/>
    <h3>Комментарий:</h3>
    <p><?=$comment->text?></p>
<hr/>
<?php if ($parentComment){ ?>
    <h3>В ответ на комментарий пользователя</h3> <?=$parentComment->user->user_profile->FullName ?>:
    <p><?=$parentComment->text?></p>
    <hr/>
<?php }?>
<br>
Редактировать/удалить этот или другие комментарии этой новости вы можете на странице:
<?= CHtml::link($comment->news->title, $this->createAbsoluteUrl("/news/view/".$comment->news->short_id))?>
</body>
</html>