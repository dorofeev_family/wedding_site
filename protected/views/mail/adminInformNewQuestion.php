<html>
<head>
</head>
<body>
Здравствуйте, <?php echo $admin->user_profile->FullName; ?>
<br>Вы получили это письмо, т.к. вы являетесь администратором сайта <?= CHtml::link(Yii::app()->name, Yii::app()->getBaseUrl(true))?>.
<br>На сайте был добавлен новый вопрос пользователем <b><?=$user->user_profile->FullName?></b>:
<br>
<hr/>
<h3>Вопрос:</h3>
<p><?=$question->question?></p>
<hr/>
<br>
Ответить/удалить этот или другие вопросы вы можете на странице: <?= CHtml::link("Вопросы и ответы", $this->createAbsoluteUrl("/questions/index/"))?>
</body>
</html>