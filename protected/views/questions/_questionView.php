<?php
/* @var $this QuestionsController */
/* @var $data Questions */
?>


<div id="<?=$data->id?>" class="single_question">
    <div class="question-box">

        <?php if ($data->canBeDeleted()){ ?>
            <div class="question_actions">
                <?php if (Yii::app()->user->checkAccess('m_question')){ ?>
                    <div class="question_edit_button">
                        <a onclick="showEditModal(<?=$data->id?>)">
                            <img src="/images/edit.png" title="Ответить/Редактировать"/>
                        </a>
                    </div>
                    <?php if ($data->status_id == 1) { ?>
                        <div class="question_approve_button">
                            <a onclick="approveQuestion(<?=$data->id?>)">
                                <img src="/images/galka.png" title="Одобрить"/>
                            </a>
                        </div>
                    <?php } else { ?>
                        <div class="question_approve_button">
                            <a onclick="undoApproveQuestion(<?=$data->id?>)">
                                <img src="/images/undo.png" title="В черновики"/>
                            </a>
                        </div>
                    <?php }?>
                <?php }?>
                <?php if ($data->canBeDeleted()){ ?>
                    <div class="question_delete_button">
                        <a onclick="deleteQuestion(<?=$data->id?>)">
                            <img src="/images/close.png" title="Удалить"/>
                        </a>
                    </div>
                <?php }?>
            </div>
        <?php }?>

        <div class="question_info_box">
            <div class="question_author">
                <h3><?=CHtml::encode($data->user->user_profile->FullName)?></h3>
            </div>

            <div class="question_date">
                <span><?=CHtml::encode($data->createdate)?></span>
            </div>

            <?php if (Yii::app()->user->checkAccess('m_question')) { ?>
                <div class="question_status">
                    <span><?=CHtml::encode($data->status->name)?></span>
                </div>
            <?php } ?>
        </div>

        <div class="question_text">
            <?=nl2br($data->question)?>
        </div>
    </div>

    <?php if ($data->answer) { ?>
        <div class="answer-box">
            <div class="answer_info_box">
                <div class="answer_author">
                    <h3><?=CHtml::encode($data->answer->respondent->user_profile->FullName)?></h3>
                </div>

                <div class="answer_date">
                    <?=CHtml::encode($data->answer->answerDate)?>
                </div>
            </div>

            <div class="answer_text">
                <?=nl2br($data->answer->answer)?>
            </div>
        </div>
    <?php } ?>

</div>