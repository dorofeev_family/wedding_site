<?php
/* @var $this QuestionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Мои вопросы',
);

Yii::app()->clientScript->registerScriptFile('/js/questions.js',CClientScript::POS_HEAD);
?>

<h1>Мои вопросы</h1>

<?php if ($dataProvider->itemCount) { ?>
    <?php
    $this->renderPartial('/questions/availableQuestions', array(
        'dataProvider' => $dataProvider
    )); ?>
<?php } else { ?>
    У вас нет заданных вопросов.
<?php } ?>