<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'edit-question-modal')); ?>
    <div class="modal-header">

        <h3>Редактирование вопроса</h3>
    </div>
    <div class="modal-body">
        <form id="edit-question-form">
            <div class="single_question">
                <div class="question-box">
                    <div class="question_info_box">
                        <input id="question_id" name="id" class="hidden"/>
                        <div class="question_author">
                            <h3 id="question_username">Пользователь</h3>
                        </div>

                        <div class="question_date">
                            <span id="question_createdate">00:00:00</span>
                        </div>
                        <div class="question_status">
                            <span>
                                <?php
                                $statuses = QuestionStatus::model()->findAll();
                                $list = CHtml::listData($statuses,'id', 'name');

                                echo CHtml::dropDownList('status', null, $list, array(
                                    'id' => 'question_status_list',
                                ));
                                ?>
                            </span>
                        </div>
                    </div>

                    <textarea id="question_text" name="question" class='question-text'></textarea>
                </div>

                <div class="answer-box">
                    <div class="answer_info_box">
                        <div class="answer_author">
                            <h3 id="answer_username"><?=Yii::app()->user->fullName?></h3>
                        </div>

                        <div class="answer_date">
                            <span id="answer_createdate">00:00:00</span>
                        </div>
                    </div>

                    <textarea id="answer_text" name="answer" class='answer-text'></textarea>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Сохранить',
            'type'=>'success',
            'htmlOptions'=>array('OnClick'=>'editQuestion()'),
        )); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Отмена',
            'type'=>'danger',
            'htmlOptions'=>array('data-dismiss'=>'modal'),
        )); ?>
    </div>
<?php $this->endWidget(); ?>