<?php
/* @var $this QuestionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Вопросы и ответы',
);

Yii::app()->clientScript->registerScriptFile('/js/questions.js',CClientScript::POS_HEAD);
?>

    <h1>Вопросы и ответы</h1>
<?php if (Yii::app()->user->checkAccess('m_question')) { ?>
    <div class="question-filter-box">
        <div class="question-filter-inner-box">
            <?=CHtml::dropDownList('question_filter_list', false, $filter, array(
                'id' => 'question_filter',
                'onChange' => 'getQuestions()'
            ))?>
        </div>
    </div>
<?php } ?>


    <div id="outer-questions-box">
        <?php $this->renderPartial('/questions/availableQuestions', array(
            'dataProvider' => $dataProvider
        )); ?>
    </div>

    <?php $this->renderPartial('/questions/_questionEdit'); ?>

    <hr/>

<?php
$this->renderPartial('_questionForm',array(
    'fakeQuestion' => $fakeQuestion
));
?>