<?php
$template = "{items}\n{pager}";

$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'template' => $template,
    'itemView'=>'/questions/_questionView',
    'pager' => array(
        'class' => 'CLinkPager',
        'header' => '',
        'htmlOptions' => array(
            'class' => 'news-pager'
        )
    ),
)); ?>