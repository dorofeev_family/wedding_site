

<div class="single_question">
    <div id="question-form-box" class="question-box">
        <div class="question_info_box">
            <div class="question_author">
                <h3><?=Yii::app()->user->fullName?></h3>
            </div>

            <div class="question_date">
                <span><?php
                    echo strftime('%d %b %Y', time());
                    ?></span>
            </div>
        </div>
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'=>'question-form',
            'enableClientValidation' => false,
            'htmlOptions'=>array(
                'onsubmit'=>"return false;"
            )
        ));

        echo $form->textArea($fakeQuestion,'question',array('class'=>'question-text'));
        ?>

        <?php
        echo CHtml::submitButton('Задать вопрос',
            array(
                'class' => 'site_button',
                'id' => 'addquestion',
                'onClick' => 'sendQuestion()'
            )
        );?>
        <?php
        $this->endWidget();
        ?>
    </div>
</div>