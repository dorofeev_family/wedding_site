<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
$this->fullsize = true;
$empty = true;
?>

<?php if ($NewNews->itemCount) { ?>
    <hr class="news-spliter"/>
    <h1 class="by-center">Последние новости</h1>
    <hr class="news-spliter"/>
    <?php $this->renderPartial('/news/availablenews', array(
        'dataProvider' => $NewNews,
        'usePagination' => false
    )); ?>
    <?php $empty = false; ?>
<?php }?>

<?php if ($MainNews->itemCount) { ?>
    <?php if (!$empty) {?>
        <hr class="news-spliter"/>
        <h2 class="by-center">Закрепленные новости</h2>
        <hr class="news-spliter"/>
    <?php }?>

    <?php $this->renderPartial('/news/availablenews', array(
        'dataProvider' => $MainNews,
        'usePagination' => false
    )); ?>
    <?php $empty = false; ?>
<?php }?>

<?php if ($empty) {?>
    <h2>Ой-ой-ой, Новостей нет!</h2>
    <p>Все новости куда-то пропали! Возможно идут какие-то работы на сайте. Зайдите попозже ;)</p>
<?php }?>










