<?php
class WebUser extends CWebUser {
    private $_model = null;

    function getRole(){
        if ($user = $this->getModel()){
            return $user->role;
        }
    }

    private function getModel(){
        if (!$this->isGuest && $this->_model === null){
            //$this->_model = User::model()->findByPk($this->id, array('select' => 'role'));
            $this->_model = User::model()->findByPk($this->id)->role;
        }
        return $this->_model;
    }

    public function isSubscribed(){
        if (isset($this->inform_me)){
            return $this->inform_me;
        }
        return false;
    }
}
?>