<?php

class CarouselWidget extends CWidget
{
    /**
     * @var CFormModel
     */
    public $form;

    public function run()
    {
        $sliders = $this->getImages();
        $this->render('carousel',array(
            'sliders' => $sliders
        ));
    }

    private function getImages(){
        $files = CFileHelper::findFiles(Yii::getPathOfAlias("webroot.images")."/slider");
        $result = array();
        foreach ($files as $key=>$path){
            array_push($result, array(
                'image'=>str_replace(Yii::getPathOfAlias("webroot"),'',$path),
                'label'=>'Image #'.$key
            ));
        }
        return $result;
    }
}

?>