<?php

class CountDownWidget extends CWidget
{
    /**
     * @var CFormModel
     */
    public $form;

    public function run()
    {
        $countdown = $this->getCountDownParams();

        if ($countdown['type'] == 'until'){
            if ($countdown['anniversary']){
                $countdown['time'] = $this->getNextDate($countdown['time']);
            }
        }

        $this->render('countdown',array(
            'countdown' => CJSON::encode($countdown)
        ));
    }

    private function getCountDownParams(){
        $variants = array(
            array(
                'time' => '06-28 15:45:00',
                'title' => 'До годовщины:',
                'expireText' => 'У нас сегодня годовщина!',
                'type' => 'until',
                'anniversary' => true
            ),
            array(
                'time' => '2013-01-01 00:00:00',
                'title' => 'Мы вместе:',
                'expireText' => '',
                'type' => 'since'
            ),
            array(
                'time' => '08-09 00:00:00',
                'title' => 'До дня рождения:',
                'expireText' => 'С днём рождения нас!',
                'type' => 'until',
                'anniversary' => true
            ),
            array(
                'time' => '2015-08-21 00:00:00',
                'title' => 'Живем в Москве уже:',
                'expireText' => '',
                'type' => 'since',
            ),
            array(
                'time' => '01-01 00:00:00',
                'title' => 'До Нового года:',
                'expireText' => 'С НОВЫМ ГОДОМ!!!',
                'type' => 'until',
                'anniversary' => true
            ),
        );

        return $variants[rand ( 0 , count($variants)-1 ) ];
    }

    private function getNextDate($date){

        $date = date("Y").'-'.$date;

        $currentDate = new DateTime();
        $matchDate  = new DateTime($date);
        $dayDiff = $currentDate->diff($matchDate);
        $diffInDays = (int)$dayDiff->format("%r%a");

        if ($diffInDays < 0){
            $matchDate->modify('+1 year');
        }

//        return $matchDate->format(DateTime::ISO8601);
        return $matchDate->format('Y-m-d H:i:s');
    }
}

?>
