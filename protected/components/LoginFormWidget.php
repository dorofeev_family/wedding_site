<?php

class LoginFormWidget extends CWidget
{
    /**
     * @var CFormModel
     */
    public $form;

    public function run()
    {
        $LoginForm = new LoginForm;
        $user = new User('create');
        $user->user_profile = new UserProfile;

        if (Yii::app()->controller->id == 'user'){
            if (Yii::app()->controller->action->id == 'login'){
                return;
            }
            if (Yii::app()->controller->action->id == 'signin'){
                return;
            }
            if (Yii::app()->controller->action->id == 'changePassword'){
                return;
            }
        }

        if (Yii::app()->user->isGuest) {
            $this->render('GuestFormWidget', array('LoginForm'=>$LoginForm,'user'=>$user));
        } else {
            $this->render('HelloFormWidget');
        }
    }

}

?>