<h2>Регистрация</h2>

<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'user-form',
        'action' => array('user/signin'),
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>

    <p class="note">Поля отмеченные <span class="required">*</span> не могут быть пустыми:</p>

    <?php echo $form->errorSummary($user); ?>

    <div class="row">
        <?php echo $form->labelEx($user,'username'); ?>
        <?php echo $form->textField($user,'username',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($user,'username'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($user,'password'); ?>
        <?php echo $form->passwordField($user,'password',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($user,'password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($user,'password_repeat'); ?>
        <?php echo $form->passwordField($user,'password_repeat',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($user,'password_repeat'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($user->user_profile,'FullName'); ?>
        <?php echo $form->textField($user->user_profile,'FullName',array('size'=>60,'maxlength'=>256)); ?>
        <?php echo $form->error($user->user_profile,'FullName'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($user->user_profile,'e_mail'); ?>
        <?php echo $form->textField($user->user_profile,'e_mail',array('size'=>60,'maxlength'=>256)); ?>
        <?php echo $form->error($user->user_profile,'e_mail'); ?>
    </div>

    <div class="row">
        <?=$form->labelEx($user->user_profile,'inform_me')?>
        <label class="user-switch switch">
            <?php echo $form->checkBox($user->user_profile,'inform_me',  array('class'=>'switch-input')); ?>
            <span class="switch-label" data-on="Да" data-off="Нет"></span>
            <span class="switch-handle"></span>
        </label>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Создать',
            array(
                'class' => 'site_button'
            )
        ); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->
