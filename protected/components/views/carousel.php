    <div id="carousel-box">
        <?php $this->widget('bootstrap.widgets.TbCarousel', array(
            'displayPrevAndNext' => false,
            'slide' => true,
            'htmlOptions'=>array('id' => 'main_carousel'),
            'items'=>$sliders,
            'options' => array(
                'interval' => 6000,
                'pause' => 'none'
            ),
        )); ?>
    </div>