<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'restore-password-modal')); ?>
    <div class="modal-header">

        <h3>Восстановление пароля</h3>
    </div>
    <div class="modal-body">
        <form id="restore-password-form">
            <input name="username" id="username" value="" placeholder="Введите ваш логин"/>
        </form>
    </div>
    <div class="modal-footer">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Восстановить',
            'type'=>'success',
            'htmlOptions'=>array('OnClick'=>'restorePassword()'),
        )); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Отмена',
            'type'=>'danger',
            'htmlOptions'=>array('data-dismiss'=>'modal'),
        )); ?>
    </div>
<?php $this->endWidget(); ?>