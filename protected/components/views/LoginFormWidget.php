<?php

Yii::app()->clientScript->registerScriptFile('/js/restorePassword.js',CClientScript::POS_HEAD);
?>
<h2>Вход на сайт</h2>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'afterValidate' => 'js:function(form, data, hasError) {
                if (!hasError){
                    str = $("#login-form").serialize() + "&ajax=login-form";

                    $.ajax({
                        type: "POST",
                        url: "' . Yii::app()->createUrl('user/login') . '",
                        data: str,
                        dataType: "json",
                        beforeSend : function() {
                            $("#login").attr("disabled",true);
                        },
                        success: function(data, status) {
                            if(data.authenticated)
                            {
                                if (data.redirectUrl){
                                    window.location = data.redirectUrl;
                                } else {
                                    location.reload();
                                }
                            }
                            else
                            {
                                $.each(data, function(key, value) {
                                    var div = "#"+key+"_em_";
                                    $(div).text(value);
                                    $(div).show();
                                });
                                $("#login").attr("disabled",false);
                            }
                        },
                    });
                    return false;
                }
            }',
        ),
    ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($LoginForm,'username'); ?>
        <?php echo $form->textField($LoginForm,'username'); ?>
        <?php echo $form->error($LoginForm,'username'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($LoginForm,'password'); ?>
        <?php echo $form->passwordField($LoginForm,'password'); ?>
        <?php echo $form->error($LoginForm,'password'); ?>
    </div>


    <div class="row rememberMe">
        <?php echo $form->checkBox($LoginForm,'rememberMe'); ?>
        <?php echo $form->label($LoginForm,'rememberMe'); ?>
        <?php echo $form->error($LoginForm,'rememberMe'); ?>
    </div>

    <div class="row buttons">

        <?php echo CHtml::submitButton('Войти',
            array(
                'class' => 'site_button',
                'id' => 'login'
            )
        ); ?>

        <?php echo CHtml::button('Забыли пароль',
            array(
                'onclick' => 'showRestorePasswordModal()',
                'class' => 'site_button',
                'id' => 'restore-password-button'
            )); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->

<?php $this->render('restorePassword',array()); ?>

