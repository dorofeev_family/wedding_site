<h2>Привет, <?=Yii::app()->user->fullName?></h2>

<div class="form">
    <ul class="menu">
        <li>
            <?php echo CHtml::link('Профиль',array('user/update','id'=>Yii::app()->user->id)); ?>
        </li>
        <li>
            <?php echo CHtml::link('Мои вопросы',array('questions/userQuestions')); ?>
        </li>
        <?php if (Yii::app()->user->inform_me) { ?>
            <li>
                <?php echo CHtml::link('Отписаться от новостей',array('user/subscribe','id'=>Yii::app()->user->id, 'subscribe'=>0)); ?>
            </li>
        <?php } else { ?>
            <li>
                <?php echo CHtml::link('Подписаться на новости',array('user/subscribe','id'=>Yii::app()->user->id, 'subscribe'=>1)); ?>
            </li>
        <?php }?>

        <li>
            <?php echo CHtml::link('Выйти',array('user/logout')); ?>
        </li>
    </ul>
</div><!-- form -->
