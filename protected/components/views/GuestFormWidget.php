<ul class="menu">
    <li class="active spoiler-element">
        <a id="login-menu-link">Вход на сайт</a>
        <div id="login-form-box">
            <?php $this->render('LoginFormWidget',array('LoginForm'=>$LoginForm)); ?>
        </div>
    </li>
    <li class="spoiler-element">
        <a id="signin-menu-link">Регистрация</a>
        <div id="signin-form-box">
            <?php $this->render('RegistrationFormWidget',array('user'=>$user)); ?>
        </div>
    </li>

</ul>

