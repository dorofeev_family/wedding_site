<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property integer $id
 * @property string $short_id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $preview
 * @property string $text
 * @property integer $sort
 * @property integer $show_on_main
 * @property string $createdate
 * @property integer $author_id
 * @property integer $status_id
 * @property integer $informed
 * @property integer $image
 *
 * The followings are the available model relations:
 * @property User $author
 * @property NewsStatus $status
 * @property NewsComment[] $newsComments
 */
class News extends CActiveRecord
{
    const PUBLIC_NEWS_STATUS_ID = 2;

    private $isNewNews;
    private $uploadedFile;
    public $deleteImageFlag = false;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'news';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('short_id, title', 'required'),
            array('sort, show_on_main, author_id, status_id, informed', 'numerical', 'integerOnly'=>true),
            array('short_id', 'length', 'max'=>128),
            array('image', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true),
            array('title, description, keywords', 'length', 'max'=>512),
            array('preview, text', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, short_id, title, preview, text, sort, show_on_main, createdate, author_id, status_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
            'status' => array(self::BELONGS_TO, 'NewsStatus', 'status_id'),
            'newsComments' => array(self::HAS_MANY, 'NewsComment', 'news_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'short_id' => 'Уникальный идентификатор',
            'title' => 'Заголовок',
            'preview' => 'Превью',
            'text' => 'Текст',
            'description' => 'Краткое описание',
            'keywords' => 'Ключевые слова',
            'sort' => 'Порядок сортировки',
            'show_on_main' => 'Закрепить на главной',
            'createdate' => 'Дата создания',
            'author_id' => 'Автор',
            'status_id' => 'Статус',
            'informed' => 'Уже информарованы',
            'image' => 'Главное изображение'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('short_id',$this->short_id,true);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('preview',$this->preview,true);
        $criteria->compare('text',$this->text,true);
        $criteria->compare('sort',$this->sort);
        $criteria->compare('show_on_main',$this->show_on_main);
        $criteria->compare('createdate',$this->createdate,true);
        $criteria->compare('author_id',$this->author_id);
        $criteria->compare('status_id',$this->status_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return News the static model class
     */

    public function beforeSave(){
        $this->isNewNews = $this->isNewRecord;
        if (!$this->author_id){
            $this->author_id = Yii::app()->user->id;
        }

        $this->uploadedFile = CUploadedFile::getInstance($this, 'image');

        if ($this->deleteImageFlag){
            $this->image = '';
        } else {
            if ($this->uploadedFile !== NULL){
                $fileName = $this->short_id."-".$this->uploadedFile;
                $this->image = $fileName;
            } else {
                unset($this->image);
            }
        }

        return true;
    }

    public function afterSave(){
        if ($this->uploadedFile){
            $this->uploadedFile->saveAs(Yii::app()->basePath.'/../images/news/'.$this->image);
        }

        $this->informIfNeeded();
        return true;
    }

    private function informIfNeeded(){
        if (!$this->informed){
            if ($this->status_id == self::PUBLIC_NEWS_STATUS_ID){
                $mSender = new MailSender();
                $mSender->sendInformMessage($this);
                return $this->setLikeInformed();
            }
        }
        return true;
    }

    private function setLikeInformed(){
        parent::afterSave();
        $this->informed = 1;
        $this->isNewRecord = false;
        return $this->saveAttributes(array('informed' => 1));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function getNotPriorityNews(){
        $criteria = array(
            'limit'=>6,
            'condition'=>'show_on_main = 0',
            'order'=>'createdate DESC'
        );
        $criteria = self::addUserAccessCondition($criteria);
        return new CActiveDataProvider('News', array(
            'criteria'=>$criteria,
            'pagination'=>false,
        ));
    }

    protected static function addUserAccessCondition($criteria){
        if (!Yii::app()->user->checkAccess('m_content')) {
            if (isset($criteria['condition'])) {
                $criteria['condition'] .= ' AND status_id >= '.self::PUBLIC_NEWS_STATUS_ID;
            } else {
                $criteria['condition'] = 'status_id >= '.self::PUBLIC_NEWS_STATUS_ID;
            }
        }
        return $criteria;
    }

    public static function getNewsToMainPage(){
        $criteria = array(
            'condition'=>'show_on_main=1',
            'order'=>'sort, createdate DESC'
        );
        $criteria = self::addUserAccessCondition($criteria);
        return new CActiveDataProvider('News', array(
            'criteria'=>$criteria,
        ));
    }

    public static function getAllNews(){
        $criteria = array(
            'order'=>'createdate DESC',
        );
        $criteria = self::addUserAccessCondition($criteria);
        return new CActiveDataProvider('News', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>6,
            ),
        ));
    }
}