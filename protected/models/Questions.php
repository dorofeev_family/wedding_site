<?php

/**
 * This is the model class for table "questions".
 *
 * The followings are the available columns in table 'questions':
 * @property integer $id
 * @property string $question
 * @property integer $user_id
 * @property string $createdate
 * @property integer $status_id
 * @property integer $answer_id
 *
 * The followings are the available model relations:
 * @property QuestionStatus $status
 * @property User $user
 */
class Questions extends CActiveRecord
{
    const PUBLIC_QUESTION_STATUS_ID = 2;
    const NO_ANSWER = 'no_answer';
    const WITH_ANSWER = 'with_answer';

    private $isItNewQuestion;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'questions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('question', 'required', 'message'=>'Текст вопроса не может быть пустым!'),
            array('user_id', 'required'),
			array('user_id, status_id, answer_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, question, user_id, createdate, status_id, answer_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'status' => array(self::BELONGS_TO, 'QuestionStatus', 'status_id'),
            'answer' => array(self::BELONGS_TO, 'Answers', 'answer_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

    public function canBeDeleted(){
        if ($this->user_id == Yii::app()->user->getId()) return true;
        if (Yii::app()->user->checkAccess('m_question')) return true;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'question' => 'Question',
			'user_id' => 'User',
			'createdate' => 'Createdate',
			'status_id' => 'Status',
			'answer_id' => 'Answer',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('question',$this->question,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('createdate',$this->createdate,true);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('answer_id',$this->answer_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Questions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave(){
        $this->isItNewQuestion = $this->isNewRecord;
        return true;
    }

    public function afterSave(){
        if ($this->answer_id) {
            if ($this->status_id == self::PUBLIC_QUESTION_STATUS_ID){
                $mSender = new MailSender();
                $mSender->sendQuestionInformMessage($this);
            }
        }

        if ($this->isItNewQuestion){
            $mSender = new MailSender();
            $mSender->sendNewQuestionInfoToAdmin($this);
        }
    }

    protected static function addUserAccessCondition($criteria){
        if (!Yii::app()->user->checkAccess('m_question')) {
            if (isset($criteria['condition'])) {
                $criteria['condition'] .= ' AND status_id >= '.self::PUBLIC_QUESTION_STATUS_ID;
            } else {
                $criteria['condition'] = 'status_id >= '.self::PUBLIC_QUESTION_STATUS_ID;
            }
        }
        return $criteria;
    }

    protected static function addConditionByType($criteria, $type){

        if (Yii::app()->user->checkAccess('m_question')) {

            if (isset($criteria['condition']))
                $criteria['condition'] = ' and ';

            switch ($type) {
                case 1:
                case 2: $criteria['condition'] = ' status_id = '.$type;
                    break;
                case self::NO_ANSWER: $criteria['condition'] = ' answer_id is null ';
                    break;
                case self::WITH_ANSWER: $criteria['condition'] = ' answer_id is not null ';
                    break;
            }
        }
        return $criteria;

    }

    public static function getAllQuestions($type = 'all'){

        $criteria = array(
            'order'=>'createdate ASC',
        );

        $criteria = self::addUserAccessCondition($criteria);
        $criteria = self::addConditionByType($criteria, $type);

        return new CActiveDataProvider('Questions', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>5,
            ),
        ));
    }

    public static function getAllUserQuestions(){
        $criteria = array(
            'order' => 'createdate ASC',
            'condition' => 'user_id = '.Yii::app()->user->id
        );
        return new CActiveDataProvider('Questions', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>5,
            ),
        ));
    }

}
