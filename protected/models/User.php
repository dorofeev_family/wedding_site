<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property integer $role_id
 * @property integer $login_code
 */
class User extends CActiveRecord
{
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';
    const ROLE_NOT_ACTIVE = 'notactive';
    const ROLE_GUEST = 'guest';

    const EMPTY_USERS = 'USER_EMPTY';
    const EMPTY_ROLE = 'ROLE_EMPTY';
    const USER_ERROR = "ERROR";
    const USER_OK = "OK";
    const DELETE_ACTIVE_USER = "DEL_ACT_USER";

    public $password_repeat = "";
    public $password_raw = "";
    private $isItNewUser;

    private $mailIsSend = false;



	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('username', 'unique', 'message'=>'Пользователь с указанным логином уже существует.' ),
            array('username, password', 'required', 'message'=>'Поле не может быть пустым.' ),

            array('password_repeat', 'required', 'on'=>array('create', 'update')),
            array('password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>'Пароли должны совпадать.', 'on'=>array('create', 'update') ),

			array('role_id', 'numerical', 'integerOnly'=>true),
			array('login_code', 'length', 'max'=>64),

			array('password, password_repeat', 'length', 'max'=>128, 'message'=>'Длинна логина не может быть больше 128 символов.' ),
            array('username', 'length', 'max'=>128, 'message'=>'Длинна пароля не может быть больше 128 символов.' ),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, password, role_id, login_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'role' => array(self::BELONGS_TO, 'Role', 'role_id'),
            'news' => array(self::HAS_MANY, 'News', 'author_id'),
            'newsComments' => array(self::HAS_MANY, 'NewsComment', 'user_id'),
            'questions' => array(self::HAS_MANY, 'Questions', 'user_id'),
            'user_profile' => array(self::HAS_ONE, 'UserProfile', 'user_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Логин',
			'password' => 'Пароль',
            'password_repeat' => 'Пароль ещё раз',
			'role_id' => 'Роль',
			'login_code' => 'Уникальный код для входа на сайт',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('role_id',$this->role_id);
		$criteria->compare('login_code',$this->login_code);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }

    public function beforeSave(){

        $this->isItNewUser = $this->isNewRecord;

        if ($this['password'] == Yii::app()->params['emptyPassword']){
            unset($this['password']);
        } else {
            if (!empty($this->password)) {
                $this->password_raw = $this->password;
                $this->password = $this->hashPassword($this->password);

            }
        }

        return true;
    }

    public function createLoginCode(){
        $this->login_code =
            md5($this->user_profile->FullName.Yii::app()->name).
            md5(time());
        if ($this->saveAttributes(array('login_code' => $this->login_code))){
            $mSender = new MailSender();
            return $mSender->sendPasswordRestoreMail($this);
        }
        return false;
    }

    public function eraseLoginCode(){
        $this->login_code = null;
        $this->saveAttributes(array('login_code' => null));
    }

    public function afterSave(){
        if ($this->isSendAfterSaveMail()){
            if ($this->isItNewUser) {
                $mSender = new MailSender();
                $mSender->sendRegistrationMail($this);
            } else {
                $mSender = new MailSender();
                $mSender->sendUpdateMail($this);
            }
        }
    }

    private function isSendAfterSaveMail(){
        if ($this->scenario === 'ChangeRole') return false;
        return true;
    }

    public function saveRelated($formData){
        $trans = Yii::app()->db->beginTransaction();

        $this->prepareDataToSave($formData);

        if($this->save()) {
            if ($this->saveProfile()){
                $trans->commit();
                return true;
            }
        }
        $trans->rollback();
        return false;
    }

    private function prepareDataToSave($formData){
        $this->attributes = $formData['User'];
        if (isset($formData['Role']))
            $this->role_id = $formData['Role']['id'];
        $this->user_profile->attributes = $formData['UserProfile'];
    }

    private function saveProfile(){
        $this->user_profile->user_id = $this->id;
        if ($this->user_profile->save()) {
            return true;
        }
        return false;
    }

    public static function changeUsersRoles($data) {

        if (!isset($data['users'])) return self::EMPTY_USERS;
        if ($data['users'] == "") return self::EMPTY_USERS;

        if ($data['role'] == "") return self::EMPTY_ROLE;

        $result = self::USER_OK;
        $mSender = new MailSender();

        $usersIDBunch = explode("," , $data['users']);
        foreach($usersIDBunch as $user_id){
            $user = User::model()->findByPk($user_id);
            $user->scenario = 'ChangeRole';
            $old_role = $user->role->name;
            $new_role = Role::model()->findByPk($data['role'])->name;
            $user->role_id = $data['role'];
            if (!$user->save()){
                $result = self::USER_ERROR;
            } else {
                $mSender->sendChangeRoleMail($user, $old_role, $new_role);
            }
        }

        return $result;
    }

    public static function deleteUsers($data) {

        if (!isset($data['users'])) return self::EMPTY_USERS;
        if ($data['users'] == "") return self::EMPTY_USERS;

        $result = self::USER_OK;

        $usersIDBunch = explode(",", $data['users']);

        foreach ($usersIDBunch as $user_id) {
            $user = User::model()->findByPk($user_id);
            if ($user->role->role === User::ROLE_NOT_ACTIVE){
                if (!$user->user_profile->delete()) {
                    $result = self::USER_ERROR;
                } else {
                    if (!$user->delete()) {
                        $result = self::USER_ERROR;
                    }
                }
            } else {
                $result = self::DELETE_ACTIVE_USER;
            }
        }

        return $result;
    }

    public function deleteUserForce()
    {
        $mSender = new MailSender();
        $mSender->sendDeleteMail($this);

        if (!$this->user_profile->delete()) {
            return false;
        } else {
            if (!$this->delete()) {
                return false;
            }
        }
    }
}
