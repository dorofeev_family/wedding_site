<?php

/**
 * This is the model class for table "news_comment".
 *
 * The followings are the available columns in table 'news_comment':
 * @property integer $id
 * @property string $text
 * @property integer $user_id
 * @property integer $news_id
 * @property integer $parent_id
 * @property string $create_date
 *
 * The followings are the available model relations:
 * @property NewsComment $parent
 * @property NewsComment[] $newsComments
 * @property User $user
 * @property News $news
 */
class NewsComment extends CActiveRecord
{

    private $isItNewComment;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news_comment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, news_id', 'required'),
            array('text','required', 'message'=>'Комментарий не может быть пустым'),
			array('user_id, news_id, parent_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, text, user_id, news_id, parent_id, create_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent' => array(self::BELONGS_TO, 'NewsComment', 'parent_id'),
			'newsComments' => array(self::HAS_MANY, 'NewsComment', 'parent_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'news' => array(self::BELONGS_TO, 'News', 'news_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'text' => 'Text',
			'user_id' => 'User',
			'news_id' => 'News',
			'parent_id' => 'Parent',
			'create_date' => 'Create Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('news_id',$this->news_id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('create_date',$this->create_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NewsComment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave(){
        $this->isItNewComment = $this->isNewRecord;
        return true;
    }

    public function afterSave(){
        if ($this->isItNewComment){
            $mSender = new MailSender();
            $mSender->sendNewCommentInfoToAdmin($this);
        }
    }

    public static function GetAllNewsComments($newsId){
        $sql = '
        select
          id,
          FullName as username,
          create_date as date,
          text,
          parent_id
        from news_comment nc
          left join user_profile us_prof on (nc.user_id = us_prof.user_id)
        where nc.news_id = :news_id
        ORDER BY date';

        $list= Yii::app()->db->createCommand($sql)->bindValue('news_id',$newsId)->queryAll();

        $allComments = array();
        foreach ($list as $comment){
            $newCommentStructure = array();
            $newCommentStructure['id'] = $comment['id'];
            $newCommentStructure['user'] = $comment['username'];
            $newCommentStructure['date'] = $comment['date'];
            $newCommentStructure['text'] = $comment['text'];
            $newCommentStructure['parent'] = $comment['parent_id'];
            $newCommentStructure['children'] = array();
            $allComments[$comment['id']] = $newCommentStructure;
        }

        $allComments = array_reverse($allComments,true);

        foreach ($allComments as $key => $comment){
            $currentParrent = $allComments[$key]['parent'];
            if ($currentParrent){
                array_unshift($allComments[$currentParrent]['children'],$allComments[$key]);
                unset($allComments[$key]);
            }
        }

        $allComments = array_reverse($allComments,true);

        return $allComments;
    }
}
