<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class MailSender extends CFormModel
{
    public $mailFrom;
    public $mailTo;
    public $subject;
    public $body;

    public static $templates = array(
        'weMoved' => 'Сообщить о переезде',
        'maintenanceMode' => 'Сообщить о технических работах'
    );

    public function rules()
    {
        return array(
            // name, email, subject and body are required
            array('mailFrom, mailTo, subject, body', 'required'),
            // email has to be a valid email address
            array('mailFrom, mailTo', 'email')
        );
    }

    public function sendWritedMail($text){
        $this->mailFrom = Yii::app()->params['adminEmail'];
        $this->subject = "Сообщение от администраторов сайта: <" . Yii::app()->name .">";
        $this->body = $text;

        $users = $this->getAllUsers();
        foreach ($users as $user){
            $this->mailTo = $user->user_profile['e_mail'];
            if (!$this->sendEmail()) return false;
        }
        return true;
    }

    public function sendTemplateMail($template){
        $this->mailFrom = Yii::app()->params['adminEmail'];
        $this->subject = "Сообщение от администраторов сайта: <" . Yii::app()->name .">";

        $users = $this->getAllUsers();
        foreach ($users as $user){
            $this->mailTo = $user->user_profile['e_mail'];
            $this->body = Yii::app()->controller->renderPartial('/mail/template/'.$template, array(
                'user' => $user,
            ), true);
            if (!$this->sendEmail()) return false;
        }
        return true;
    }

    public function sendRegistrationMail($user){
        $this->mailFrom = Yii::app()->params['adminEmail'];
        $this->mailTo = $user->user_profile['e_mail'];
        $this->subject = "Вы зарегистрировались на сайте: <" . Yii::app()->name .">";
        $this->body = Yii::app()->controller->renderPartial('/mail/newuser', array('user' => $user), true);
        return $this->sendEmail();
    }

    public function sendPasswordRestoreMail($user){
        $this->mailFrom = Yii::app()->params['adminEmail'];
        $this->mailTo = $user->user_profile['e_mail'];
        $this->subject = "Восстановление пароля на сайте: <" . Yii::app()->name .">";
        $this->body = Yii::app()->controller->renderPartial('/mail/restorePassword', array('user' => $user), true);
        return $this->sendEmail();
    }


    public function sendInformMessage($news){
        $this->mailFrom = Yii::app()->params['adminEmail'];
        $this->subject = "Обновления на сайте: <" . Yii::app()->name .">";

        $users = $this->getAllSubscribedUsers();
        foreach ($users as $user){
                $this->mailTo = $user->user_profile['e_mail'];
                $this->body = Yii::app()->controller->renderPartial('/mail/newNews', array(
                    'news' => $news,
                    'user' => $user,
                ), true);
                if (!$this->sendEmail()) return false;
        }

        return true;
    }

    public function sendQuestionInformMessage($question){
        $question = Questions::model()->findByPk($question->id);
        $user = User::model()->findByPk($question->user_id);
        if ($this->isUserAskForInform($user)) {
            $this->mailFrom = Yii::app()->params['adminEmail'];
            $this->subject = "Получен ответ на ваш вопрос на сайте: <" . Yii::app()->name .">";
            $this->mailTo = $user->user_profile['e_mail'];
            $this->body = Yii::app()->controller->renderPartial('/mail/questionInform', array(
                'question' => $question,
                'user' => $user,
            ), true);
            return $this->sendEmail();
        }
        return true;
    }

    public function sendNewQuestionInfoToAdmin($question){
        $this->mailFrom = Yii::app()->params['adminEmail'];
        $user = User::model()->findByPk($question->user_id);

        $this->subject = "Новый вопрос на сайте: <" . Yii::app()->name .">";

        $admins = $this->getAllAdmins();

        foreach ($admins as $admin){
            $this->mailTo = $admin->user_profile->e_mail;
            $this->body = Yii::app()->controller->renderPartial('/mail/adminInformNewQuestion', array(
                'admin' => $admin,
                'user' => $user,
                'question' => $question,
            ), true);

            if (!$this->sendEmail()) return false;
        }

        return true;
    }


    public function sendNewCommentInfoToAdmin($comment){
        $this->mailFrom = Yii::app()->params['adminEmail'];

        $parentComment = false;
        if ($comment->parent_id){
            $parentComment = NewsComment::model()->findByPk($comment->parent_id);
        }

        $this->subject = "Новый комментарий на сайте: <" . Yii::app()->name .">";

        $admins = $this->getAllAdmins();

        foreach ($admins as $admin){
            $this->mailTo = $admin->user_profile->e_mail;
            $this->body = Yii::app()->controller->renderPartial('/mail/adminInformNewComment', array(
                'admin' => $admin,
                'comment' => $comment,
                'parentComment' => $parentComment
            ), true);

            if (!$this->sendEmail()) return false;
        }

        return true;
    }

    private function getAllAdmins(){
        return User::model()->with('role', 'user_profile')->findAll('role.role = "'.User::ROLE_ADMIN.'"');
    }

    private function getAllUsers(){
        return User::model()->findAll();
    }

    private function getAllSubscribedUsers(){
        return User::model()->with('user_profile')->findAll('inform_me = 1');
    }


    public function sendUpdateMail($user){
        if ($this->isUserAskForInform($user)) {
            $this->mailFrom = Yii::app()->params['adminEmail'];
            $this->mailTo = $user->user_profile['e_mail'];
            $this->subject = "Ваши данные на сайте <" . Yii::app()->name . "> были изменены";
            $this->body = Yii::app()->controller->renderPartial('/mail/updateuser', array('user' => $user), true);
            return $this->sendEmail();
        }
    }

    public function sendChangeRoleMail($user, $old_status, $new_status){
        if ($this->isUserAskForInform($user)) {
            $this->mailFrom = Yii::app()->params['adminEmail'];
            $this->mailTo = $user->user_profile['e_mail'];
            $this->subject = "Ваш статус на сайте <" . Yii::app()->name . "> был изменен.";
            $this->body = Yii::app()->controller->renderPartial('/mail/newstatus', array('user' => $user, 'old_status' => $old_status, 'new_status' => $new_status), true);
            return $this->sendEmail();
        }
    }

    public function sendDeleteMail($user){
        if ($user->role->role != User::ROLE_NOT_ACTIVE){
            $this->mailFrom = Yii::app()->params['adminEmail'];
            $this->mailTo = $user->user_profile['e_mail'];
            $this->subject = "Ваш аккаунт на сайте <" . Yii::app()->name . "> был удален.";
            $this->body = Yii::app()->controller->renderPartial('/mail/deleteuser', array('user' => $user), true);
            return $this->sendEmail();
        }
    }

    private function isUserAskForInform($user){
        return $user->user_profile->inform_me;
    }

    public function sendEmail(){
        if ($this->validate()) {
            $headers =
                "From: ".Yii::app()->params['adminNameInEmail']." <".Yii::app()->params['adminEmail'].">\r\n".
                "Reply-To: ".Yii::app()->params['adminNameInEmail']." <".Yii::app()->params['adminEmail'].">\r\n".
                "MIME-Version: 1.0\r\n".
                "Content-Type: text/html; charset=utf-8 \r\n";
                //"Content-Transfer-Encoding: base64 \r\n";
            //$this->subject = '=?UTF-8?B?'. base64_encode($this->subject). '?=';
            return mail($this->mailTo, $this->subject, $this->body, $headers);
        } else {
            return false;
        }
    }
}
