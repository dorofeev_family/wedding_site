<?php

// change the following paths if necessary
$yii=dirname(__FILE__).'/../frameworks/yii1/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1'){
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
    define('LOCAL',true);
} else {
    define('LOCAL',false);
    defined('YII_DEBUG') or define('YII_DEBUG',false);
}
// specify how many levels of call stack should be shown in each log message


require_once($yii);
Yii::createWebApplication($config)->run();
